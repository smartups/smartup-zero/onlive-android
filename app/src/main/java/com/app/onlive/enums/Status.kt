package com.app.onlive.enums

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}