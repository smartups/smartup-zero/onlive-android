package com.app.onlive.enums

enum class CardState(val title: String) {
    SAVED("SAVED"),
    PUBLISHED("PUBLISHED"),
}