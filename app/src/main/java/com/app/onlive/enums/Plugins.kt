package com.app.onlive.enums

enum class Plugins(
    val id: String,
    val pluginIcon: String,
    val bgColor: String,
    val lightBgColor: String
) {
    DOCUMENT(
        "DOCUMENT",
        "https://firebasestorage.googleapis.com/v0/b/onlive-smartup-zero.appspot.com/o/plugins%2FFile%20Icon.png?alt=media&token=cf904f0a-74ad-4982-bd73-a5e029bebb8a",
        "#008738",
        "#6D9F71"
    ),
    ONLIVEID(
        "ONLIVEID",
        "https://firebasestorage.googleapis.com/v0/b/onlive-smartup-zero.appspot.com/o/plugins%2FID%20card%20icon.png?alt=media&token=6249d2b0-270e-4f1a-b984-aea5fc5ff568",
        "#006E90",
        "#23B5D3"
    ),
    QUESTIONNAIRE(
        "QUESTIONNAIRE",
        "https://www.ipsos.com/sites/default/files/inline-images/questionnaire.jpg",
        "#FFC61B",
        "#CCCC00"
    )
}