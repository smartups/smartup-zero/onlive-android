package com.app.onlive.enums

enum class DocumentTypes(val title: String) {
    PDF("pdf"),
    WEB("web"),
    IMAGE("image"),
}