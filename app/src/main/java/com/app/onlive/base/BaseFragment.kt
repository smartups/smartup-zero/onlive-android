package com.app.onlive.base

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.app.onlive.prefences.PrefManager
import com.app.onlive.utils.Constants
import com.app.onlive.utils.DialogUtils
import com.app.onlive.view.activities.LoginActivity
import com.app.onlive.view.activities.MainActivity
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.java.KoinJavaComponent

abstract class BaseFragment : Fragment() {

    protected lateinit var mainActivity: MainActivity
    protected lateinit var progressDialog: KProgressHUD
    protected val prefManager: PrefManager by KoinJavaComponent.inject(PrefManager::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
        progressDialog = DialogUtils.showProgressDialog(mainActivity, cancelable = false)
    }

    protected fun leaveGroup() {
        prefManager.deleteSpec(Constants.EXTRA_GROUP_ID)
        startActivity(Intent(mainActivity, LoginActivity::class.java))
        mainActivity.finish()
    }
}