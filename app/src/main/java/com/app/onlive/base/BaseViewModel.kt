package com.app.onlive.base

import androidx.lifecycle.ViewModel
import com.app.onlive.prefences.PrefManager
import org.koin.java.KoinJavaComponent

/**
 * An abstract Base View model to share common behavior and additionally clear navigation reference upon invalidation.
 */
abstract class BaseViewModel<View, Repo> : ViewModel() {

    protected val prefManager: PrefManager by KoinJavaComponent.inject(PrefManager::class.java)

    private var view: View? = null
    /**
     * This method must be called by the UI to attach navigation to be monitored by the substituted view model to respond to UI specific event changes.
     * @param navigator reference to navigation
     */
    open fun attachView(view: View) {
        this.view = view
    }

    /**
     * @returns current navigator if attached
     */
    protected fun getViewNavigator(): View? {
        return view
    }

    abstract fun setAppRepository(): Repo
    protected fun getAppRepository(): Repo {
        if (setAppRepository() == null)
            throw NullPointerException("Repo is null please attach Repo 1st")
        return setAppRepository()!!
    }

    override fun onCleared() {
        view = null
    }
}