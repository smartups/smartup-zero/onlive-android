package com.app.onlive.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.onlive.databinding.LayoutInternetMessageBinding
import com.app.onlive.extensions.gone
import com.app.onlive.extensions.visible
import com.app.onlive.prefences.PrefManager
import com.app.onlive.utils.DialogUtils
import com.app.onlive.utils.InternetMonitor
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.java.KoinJavaComponent.inject

abstract class BaseActivity : AppCompatActivity() {

    protected lateinit var progressDialog: KProgressHUD
    private lateinit var internetMessageBinding: LayoutInternetMessageBinding

    protected val prefManager: PrefManager by inject(PrefManager::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        internetMessageBinding = LayoutInternetMessageBinding.inflate(layoutInflater)

        progressDialog = DialogUtils.showProgressDialog(this, cancelable = false)
    }

    override fun onResume() {
        super.onResume()
        internetMonitor.register(this)
    }

    override fun onPause() {
        super.onPause()
        internetMonitor.unregister(this)
    }

    //====================== Internet monitor handling ========================//

    private val internetMonitor = InternetMonitor(object :
        InternetMonitor.OnInternetConnectivityListener {
        override fun onInternetAvailable() {
            runOnUiThread {
                internetMessageBinding.topConnectionShower.gone()
            }
        }

        override fun onInternetLost() {
            runOnUiThread {
                internetMessageBinding.topConnectionShower.visible()
            }
        }
    })
}