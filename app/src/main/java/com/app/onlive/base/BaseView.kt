package com.app.onlive.base


interface BaseView {
    fun showProgressBar()
    fun dismissProgressBar()
    fun onDetailsUpdateError(error: String)
}