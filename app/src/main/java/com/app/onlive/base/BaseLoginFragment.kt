package com.app.onlive.base

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.app.onlive.prefences.PrefManager
import com.app.onlive.utils.DialogUtils
import com.app.onlive.view.activities.LoginActivity
import com.app.onlive.view.activities.MainActivity
import com.kaopiz.kprogresshud.KProgressHUD
import org.koin.java.KoinJavaComponent

abstract class BaseLoginFragment : Fragment() {

    protected lateinit var loginActivity: LoginActivity
    protected lateinit var progressDialog: KProgressHUD
    protected val prefManager: PrefManager by KoinJavaComponent.inject(PrefManager::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginActivity = activity as LoginActivity
        progressDialog = DialogUtils.showProgressDialog(loginActivity, cancelable = false)
    }

    protected fun goToMainActivity() {
        startActivity(Intent(loginActivity, MainActivity::class.java))
        loginActivity.finish()
    }
}