package com.app.onlive

import android.app.Application
import android.content.Context
import com.app.onlive.prefences.PrefManager
import com.app.onlive.repository.PlatformRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        initDependencyInjection()
    }

    companion object {
        var instance: App? = null
        fun getAppContext(): Context {
            return instance as Context
        }
    }

    private fun initDependencyInjection() {
        val myModule = module {
            single { PlatformRepository.getInstance() }
            single { PrefManager.getInstance() }
        }

        // start Koin!
        startKoin {
            // declare used Android context
            androidContext(applicationContext)
            // declare modules
            modules(myModule)
        }
    }
}