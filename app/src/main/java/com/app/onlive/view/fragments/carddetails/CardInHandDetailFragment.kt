package com.app.onlive.view.fragments.carddetails

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCreatedCardBinding
import com.app.onlive.enums.CardState
import com.app.onlive.enums.Plugins
import com.app.onlive.extensions.*
import com.app.onlive.models.response.CardsResponse
import com.app.onlive.models.response.GroupsResponse
import com.app.onlive.utils.Constants.CARD_ID
import com.app.onlive.utils.Constants.CARD_MODEL
import com.app.onlive.view.fragments.card.CardsStageFragment
import com.app.onlive.view.fragments.card.CardsStageViewModel
import com.app.onlive.view.fragments.plugins.PluginSelectionFragment
import com.app.onlive.view.fragments.plugins.showplugins.showdocument.ShowDocumentCardFragment
import com.app.onlive.view.fragments.plugins.showplugins.showidcard.ShowIdCardFragment
import com.app.onlive.view.fragments.plugins.showplugins.showquestionecard.ShowQuestionerCardFragment

class CardInHandDetailFragment : BaseFragment(), View.OnClickListener,
    CardInHandDetailViewModel.View, CardsStageViewModel.View {

    private lateinit var binding: FragmentCreatedCardBinding
    private val viewModel: CardInHandDetailViewModel by viewModels()
    private var cardDetails: CardsResponse? = null
    private var cardId: String? = null
    private var groupDetails: GroupsResponse? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreatedCardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)
        cardDetails =
            arguments?.getParcelable(CARD_MODEL) as? CardsResponse

        cardId = arguments?.getString(CARD_ID)

        when {
            cardDetails != null -> {
                populateUI()  // while creating the card, data from last screen
            }
            cardId.isNullOrBlank()
                .not() -> {  // while picking the card from stag/saved cards to in hand, data from firebase
                viewModel.getCardDetails(cardId!!)
            }
            cardDetails == null && cardId == null -> {
                mainActivity.backPress()
                return
            }
        }

        viewModel.updateDeleteButtonText(cardDetails!!) {
            binding.tvAbortCard.text = it
        }

        for (plugin in Plugins.values()) {
            if (cardDetails?.cardPluginId == plugin.id) {
                binding.layoutCard.background
                    .setTint(Color.parseColor(plugin.bgColor))
                binding.layoutCreaterName.setBackgroundColor(Color.parseColor(plugin.lightBgColor))
            }
        }

        if (cardDetails?.isCardShareAble!!.not()) {
            binding.downloadable.gone()
            binding.notDownloadable.visible()
        } else {
            binding.downloadable.visible()
            binding.notDownloadable.gone()
        }

        binding.tvPublishCard.setOnClickListener(this)
        binding.tvAbortCard.setOnClickListener(this)
        binding.tvSaveCard.setOnClickListener(this)
        binding.layoutCard.setOnClickListener(this)
        binding.tvAbortCard.setOnClickListener(this)

        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@CardInHandDetailFragment)
            binding.ivBtHome.setOnClickListener(this@CardInHandDetailFragment)
            binding.ivBtHome.visible()
        }

        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }
    }

    private fun populateUI() {
        cardDetails?.let {
            binding.ivCardImg.loadImage(it.cardIcon)
            binding.tvTitle.text = it.cardTitle
            binding.tvByName.text = it.createdBy
            binding.civPlugin.loadImage(it.cardPluginIcon)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.tvAbortCard -> {
                cardDetails ?: mainActivity.backPress()
                cardDetails ?: return

                viewModel.onCardCancelOrDeletion(cardDetails!!)
            }
            R.id.iv_bt_home -> replaceFragment(
                PluginSelectionFragment(
                    false,
                    groupDetails?.selectedPluginList
                ), addToBackStack = true
            )
            R.id.tvPublishCard -> {
                cardDetails ?: return
                viewModel.createCard(cardDetails!!, CardState.PUBLISHED)
            }
            R.id.tvSaveCard -> {
                cardDetails ?: return
                viewModel.createCard(cardDetails!!, CardState.SAVED)
            }
            R.id.layoutCard -> {
                when (cardDetails?.cardPluginId) {
                    Plugins.ONLIVEID.id -> {
                        replaceFragment(
                            ShowIdCardFragment(),
                            addToBackStack = true,
                            bundle = Bundle().also {
                                it.putString(CARD_ID, cardDetails?.cardId)
                            })
                    }
                    Plugins.DOCUMENT.id -> {
                        replaceFragment(
                            ShowDocumentCardFragment(),
                            addToBackStack = true,
                            bundle = Bundle().also {
                                it.putString(CARD_ID, cardDetails?.cardId)
                            })
                    }
                    Plugins.QUESTIONNAIRE.id -> {
                        replaceFragment(
                            ShowQuestionerCardFragment(),
                            addToBackStack = true,
                            bundle = Bundle().also {
                                it.putString(CARD_ID, cardDetails?.cardId)
                            })
                    }
                }
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onFindingCards(cards: List<CardsResponse>) {

    }

    override fun onCardCreation(message: String, documentId: String) {
        showToastMsg(message)
        replaceFragment(CardsStageFragment(), addToBackStack = false)
    }

    override fun onCardDetails(cardDetails: CardsResponse) {
        this.cardDetails = cardDetails
        populateUI()
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    override fun onCardDeletion() {
        mainActivity.backPress()
    }

    override fun onGroupDetails(group: GroupsResponse) {
        groupDetails = group
        mainActivity.binding.appBar.userProfileImage.loadImage(groupDetails?.icon)
    }
}