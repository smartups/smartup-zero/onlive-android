package com.app.onlive.view.fragments.signup

import android.net.Uri
import com.app.onlive.base.BaseViewModel
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.view.activities.LoginActivity
import com.google.firebase.auth.FirebaseUser

class SignupViewModel : BaseViewModel<SignupViewModel.View, PlatformRepository>() {

    override fun setAppRepository() = PlatformRepository.getInstance()

    fun signup(loginActivity: LoginActivity, email: String, password: String) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().createAccount(loginActivity, email, password) { auth, error ->
            getViewNavigator()?.dismissProgressBar()
            if (auth != null)
                getViewNavigator()?.onSignupSuccess(auth, "User created successfully, login now")
            else
                getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    fun uploadImage(uri: Uri, onSuccess: (imgPath: String) -> Unit) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().uploadFileToFireBase(uri) { imgPath, error ->
            getViewNavigator()?.dismissProgressBar()
            imgPath?.let {
                onSuccess.invoke(it)
            } ?: getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    fun createProfile(profileImage: String?, email: String) {
        if (profileImage.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Please select profile picture")
            return
        }

        getViewNavigator()?.showProgressBar()
        val hashMap = HashMap<String, Any>()
        hashMap["userImage"] = profileImage
        hashMap["userEmail"] = email

        getAppRepository().updateDataInFireBase(
            DOCUMENT_PATH, getAppRepository().getUserAuth().uid.toString(),
            hashMap
        ) { isSuccess, message ->
            getViewNavigator()?.dismissProgressBar()
            if (isSuccess.not())
                getViewNavigator()?.onDetailsUpdateError("some thing went wrong")
            else
                getViewNavigator()?.onProfileCreation("operation successful")
        }
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onSignupSuccess(firebaseUser: FirebaseUser, message: String)
        fun onDetailsUpdateError(message: String)
        fun onProfileCreation(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH = "profile"
    }
}