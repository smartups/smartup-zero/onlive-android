package com.app.onlive.view.fragments.plugins.questionerplugin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCreateQuestionerPluginBinding
import com.app.onlive.extensions.backPress
import com.app.onlive.extensions.gone
import com.app.onlive.extensions.replaceFragment
import com.app.onlive.extensions.showToastMsg
import com.app.onlive.utils.Constants.PLUGIN_MODEL
import com.app.onlive.view.fragments.createcard.CardCreationFragment

class QuestionerPluginFragment : BaseFragment(), QuestionerPluginViewModel.View,
    View.OnClickListener {

    private lateinit var binding: FragmentCreateQuestionerPluginBinding
    private val viewModel: QuestionerPluginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateQuestionerPluginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@QuestionerPluginFragment)
            binding.ivBtHome.gone()
        }
        binding.btnSubmit.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.btnSubmit -> {
                viewModel.createQuestionerCard(
                    questionerQuestion = binding.etQuestionerDetails.text.toString(),
                    questionerOption1 = binding.etOption1.text.toString(),
                    questionerOption2 = binding.etOption2.text.toString(),
                    questionerOption3 = binding.etOption3.text.toString(),
                    questionerOption4 = binding.etOption4.text.toString()
                )
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDataValidation(pluginDetails: HashMap<String, Any>) {
        replaceFragment(CardCreationFragment(), addToBackStack = false, bundle = Bundle().also {
            it.putSerializable(PLUGIN_MODEL, pluginDetails)
        })
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }
}