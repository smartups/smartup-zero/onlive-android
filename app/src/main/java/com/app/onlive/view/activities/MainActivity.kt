package com.app.onlive.view.activities

import android.os.Bundle
import android.view.View
import com.app.onlive.R
import com.app.onlive.base.BaseActivity
import com.app.onlive.databinding.ActivityMainBinding
import com.app.onlive.extensions.replaceFragmentSafely
import com.app.onlive.utils.Constants
import com.app.onlive.view.fragments.card.CardsStageFragment
import com.app.onlive.view.fragments.home.BottomNavFragment
import com.app.onlive.view.fragments.savedcard.SavedCardsFragment

class MainActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        binding = ActivityMainBinding.inflate(layoutInflater).also {
            setContentView(it.root)
        }

        binding.appBar.imgSavedCards.setOnClickListener(this)

        if (prefManager.getStringPref(Constants.EXTRA_GROUP_ID, null).isNullOrBlank().not()) {
            replaceFragmentSafely(CardsStageFragment())
        } else
            replaceFragmentSafely(BottomNavFragment())
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgSavedCards -> replaceFragmentSafely(SavedCardsFragment(), addToBackStack = true)
        }
    }
}