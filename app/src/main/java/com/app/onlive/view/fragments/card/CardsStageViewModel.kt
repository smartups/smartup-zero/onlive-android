package com.app.onlive.view.fragments.card

import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.CardState
import com.app.onlive.enums.Plugins
import com.app.onlive.models.response.*
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.view.fragments.carddetails.CardInHandDetailViewModel.Companion.getQuestionerCardOptions

class CardsStageViewModel : BaseViewModel<CardsStageViewModel.View, PlatformRepository>() {

    private var idCardResponse: IdCardResponse = IdCardResponse()
    private var documentCardResponse: DocumentCardResponse = DocumentCardResponse()
    private var questionerCardResponse: QuestionerCardResponse = QuestionerCardResponse()

    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getGroupDetails(groupId: String) {
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PATH_GROUPS,
            groupId
        ) { document, error ->
            if (error.isNullOrBlank()) {
                document?.let {
                    val group = GroupsResponse(
                        id = it.id,
                        title = it.data?.get("title").toString(),
                        details = it.data?.get("details").toString(),
                        icon = it.data?.get("icon").toString(),
                        selectedPluginList = it.data?.get("plugins") as ArrayList<String>,
                        createdBy = it.data?.get("createdBy").toString()

                    )
                    getViewNavigator()?.onGroupDetails(group)
                }
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    fun getAllAvailableCards(groupId: String) {
        setAppRepository().getDataFromFireBase(
            DOCUMENT_PATH_CARDS,
            searchKey = DOCUMENT_GROUP_ID,
            searchValue = groupId
        ) { querySnapshot, error ->
            if (error.isNullOrBlank()) {

                val cards = ArrayList<CardsResponse>()
                querySnapshot?.forEach {
                    if (it.data["cardGroupId"] == groupId && it.data["cardState"].toString() == CardState.PUBLISHED.name) {
                        when (it.data["cardPluginId"].toString()) {
                            Plugins.ONLIVEID.id -> {
                                idCardResponse.apply {
                                    idCardName = it.data["idCardName"].toString()
                                    idCardPhone = it.data["idCardPhone"].toString()
                                    idCardEmail = it.data["idCardEmail"].toString()
                                    idCardAddress = it.data["idCardAddress"].toString()
                                    idCardImage = it.data["idCardImage"].toString()
                                }
                            }
                            Plugins.DOCUMENT.id -> {
                                documentCardResponse.apply {
                                    documentDetails = it.data["documentDetails"].toString()
                                    documentPathURL = it.data["documentPathURL"].toString()
                                    documentExtensionType =
                                        it.data["documentExtensionType"].toString()
                                }
                            }
                            Plugins.QUESTIONNAIRE.id -> {
                                questionerCardResponse.apply {
                                    questionerQuestion =
                                        it.data["questionerCardResponse"].toString()

                                    questionerOption1 = getQuestionerCardOptions(
                                        key = "questionerOption1",
                                        hashMap = it.data["questionerOption1"] as HashMap<*, *>
                                    )
                                    questionerOption2 = getQuestionerCardOptions(
                                        key = "questionerOption2",
                                        hashMap = it.data["questionerOption2"] as HashMap<*, *>
                                    )
                                    questionerOption3 = getQuestionerCardOptions(
                                        key = "questionerOption3",
                                        hashMap = it.data["questionerOption3"] as HashMap<*, *>
                                    )
                                    questionerOption4 = getQuestionerCardOptions(
                                        key = "questionerOption4",
                                        hashMap = it.data["questionerOption4"] as HashMap<*, *>
                                    )
                                }
                            }
                        }

                        cards.add(
                            CardsResponse(
                                cardId = it.id,
                                createdBy = it.data["createdBy"].toString(),
                                cardTitle = it.data["cardTitle"].toString(),
                                cardIcon = it.data["cardIcon"].toString(),
                                isCardShareAble = it.data["isCardShareAble"] as Boolean,
                                cardState = it.data["cardState"].toString(),
                                cardGroupId = it.data["cardGroupId"].toString(),
                                cardPluginId = it.data["cardPluginId"].toString(),
                                cardPluginIcon = it.data["cardPluginIcon"].toString(),
                                cardCreatorId = it.data["cardCreatorId"].toString(),
                                idCardResponse = idCardResponse,
                                documentCardResponse = documentCardResponse,
                                questionerCardResponse = questionerCardResponse
                            )
                        )
                    }
                }
                getViewNavigator()?.onFindingCards(cards)
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }


    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onFindingCards(cards: List<CardsResponse>)
        fun onGroupDetails(group: GroupsResponse)
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH_GROUPS = "groups"
        private const val DOCUMENT_PATH_CARDS = "cards"
        private const val DOCUMENT_GROUP_ID = "groupId"
    }
}