package com.app.onlive.view.fragments.plugins.idcard

import android.net.Uri
import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.Plugins
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.utils.Constants

class IdCardPluginViewModel : BaseViewModel<IdCardPluginViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun uploadImage(uri: Uri, onSuccess: (imgPath: String) -> Unit) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().uploadFileToFireBase(uri) { imgPath, error ->
            getViewNavigator()?.dismissProgressBar()
            imgPath?.let {
                onSuccess.invoke(it)
            } ?: getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    fun createUserId(
        name: String?,
        phone: String?,
        email: String?,
        address: String?,
        image: String?
    ) {
        if (name.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Name is required")
            return
        }

        if (phone.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Phone number is required")
            return
        }

        if (email.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Email address is required")
            return
        }

        if (image.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("ID image is required")
            return
        }

        val hashMap = HashMap<String, Any>()
        hashMap["idCardName"] = name
        hashMap["idCardPhone"] = phone
        hashMap["idCardEmail"] = email
        hashMap["idCardImage"] = image
        hashMap["idCardAddress"] = address ?: ""

        hashMap["cardPluginId"] = Plugins.ONLIVEID.id
        hashMap["cardPluginIcon"] = Plugins.ONLIVEID.pluginIcon
        hashMap["cardGroupId"] = prefManager.getStringPref(Constants.EXTRA_GROUP_ID, null).toString()
        hashMap["cardCreatorId"] = getAppRepository().getUserAuth().currentUser?.uid.toString()

        getViewNavigator()?.onDataValidation(hashMap)
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onDataValidation(pluginDetails: HashMap<String, Any>)
        fun onDetailsUpdateError(message: String)
    }
}