package com.app.onlive.view.fragments.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.onlive.base.BaseLoginFragment
import com.app.onlive.databinding.SplashFragmentBinding
import com.app.onlive.extensions.replaceFragment
import com.app.onlive.utils.Constants
import com.app.onlive.view.activities.LoginActivity
import com.app.onlive.view.fragments.card.CardsStageFragment
import com.app.onlive.view.fragments.login.LoginFragment

class SplashFragment : BaseLoginFragment() {

    private lateinit var binding: SplashFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SplashFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Handler(Looper.getMainLooper()).postDelayed({
            when {
                prefManager.isUserLoggedOut() -> {
                    replaceFragment(LoginFragment(), addToBackStack = false)
                }
                else -> {
                    goToMainActivity()
                }
            }
        }, SPLASH_DURATION)

    }

    companion object {
        const val SPLASH_DURATION = 2000L
    }
}