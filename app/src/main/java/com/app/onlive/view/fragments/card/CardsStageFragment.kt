package com.app.onlive.view.fragments.card

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.adapter.CardsAdapter
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCardListBinding
import com.app.onlive.extensions.*
import com.app.onlive.models.response.CardsResponse
import com.app.onlive.models.response.GroupsResponse
import com.app.onlive.utils.Constants.CARD_MODEL
import com.app.onlive.utils.Constants.EXTRA_GROUP_ID
import com.app.onlive.utils.Constants.QRCODE_INPUT
import com.app.onlive.utils.Constants.USER_ID
import com.app.onlive.view.fragments.carddetails.CardInHandDetailFragment
import com.app.onlive.view.fragments.generateqrcode.ShowQRCodeFragment
import com.app.onlive.view.fragments.plugins.PluginSelectionFragment
import com.app.onlive.view.fragments.savedcard.SavedCardsFragment


class CardsStageFragment : BaseFragment(), CardsAdapter.OnCardClickListener,
    View.OnClickListener, CardsStageViewModel.View {

    private lateinit var binding: FragmentCardListBinding
    private val viewModel: CardsStageViewModel by viewModels()
    private var groupDetails: GroupsResponse? = null
    private lateinit var cardsAdapter: CardsAdapter
    private val TIME_INTERVAL =
        2000 // # milliseconds, desired time passed between two back presses.
    private var onBackPressed: Long = 0
    var groupId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCardListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        groupId = prefManager.getStringPref(EXTRA_GROUP_ID, null)

        if (groupId == null) {
            showToastMsg("Something went wrong")
            mainActivity.backPress()
            return
        }

        with(viewModel) {
            attachView(this@CardsStageFragment)
            getAllAvailableCards(groupId!!)
            getGroupDetails(groupId!!)
        }

        PluginSelectionFragment.selectedPluginList.clear()
        cardsAdapter = CardsAdapter(this)
        binding.rvCards.adapter = cardsAdapter

        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@CardsStageFragment)
            binding.ivBtHome.setOnClickListener(this@CardsStageFragment)
            binding.ivBtHome.visible()
            onBackPressedDispatcher.addCallback(this) {
                onDoubleBackPressedListener()
            }
        }

        binding.layoutSwiper.setOnRefreshListener {
            viewModel.getAllAvailableCards(groupId!!)
        }
        // Configure the refreshing colors
        binding.layoutSwiper.setColorSchemeResources(
            R.color.primary,
            R.color.yellow,
            R.color.green_dark
        )
    }

    private fun stageBackDialogue() {
        mainActivity.showOptionsSelectionDialogue(
            arrayOf(
                "Create card", "Share group", "Exit group"
            )
        ) {
            when (it) {
                0 -> {//create new card
                    replaceFragment(
                        PluginSelectionFragment(
                            false,
                            groupDetails?.selectedPluginList
                        ), addToBackStack = true
                    )
                }
                1 -> {
                    replaceFragment(
                        ShowQRCodeFragment(),
                        addToBackStack = true,
                        bundle = Bundle().also { bundle ->
                            bundle.putString(QRCODE_INPUT, groupId!!)
                        })
                }
                else -> {
                    leaveGroup()
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_bt_home -> {
                stageBackDialogue()
            }
            R.id.btn_back -> onDoubleBackPressedListener()
            R.id.imgSavedCards -> replaceFragment(SavedCardsFragment(), true)
        }
    }

    override fun clickListener(card: CardsResponse) {
        replaceFragment(CardInHandDetailFragment(), addToBackStack = true, bundle = Bundle().also {
            it.putParcelable(CARD_MODEL, card)
        })
    }

    override fun onFindingCards(cards: List<CardsResponse>) {
        binding.layoutSwiper.isRefreshing = false
        cardsAdapter.setItems(cards)
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onGroupDetails(group: GroupsResponse) {
        groupDetails = group
        mainActivity.binding.appBar.userProfileImage.loadImage(group.icon)
        if (group.createdBy == prefManager.getStringPref(USER_ID, "")) {
            mainActivity.binding.appBar.layoutAppbar.setBackgroundColor(resources.getColor(R.color.primary))
            binding.layoutSwiper.setBackgroundColor(resources.getColor(R.color.primary))
        }
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    private fun onDoubleBackPressedListener() {
        if (onBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            leaveGroup()
        } else {
            showToastMsg("Tap back button in order to leave group")
        }
        onBackPressed = System.currentTimeMillis()
    }
}