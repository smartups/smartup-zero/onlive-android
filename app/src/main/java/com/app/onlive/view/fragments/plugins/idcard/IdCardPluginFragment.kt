package com.app.onlive.view.fragments.plugins.idcard

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCreateIdCardPluginBinding
import com.app.onlive.extensions.*
import com.app.onlive.utils.Constants.IS_IMAGE_VIDEO
import com.app.onlive.utils.Constants.PLUGIN_MODEL
import com.app.onlive.utils.FileUriUtils
import com.app.onlive.view.activities.MainActivity
import com.app.onlive.view.fragments.createcard.CardCreationFragment
import com.permissionx.guolindev.PermissionX
import java.io.File

class IdCardPluginFragment : BaseFragment(), IdCardPluginViewModel.View,
    View.OnClickListener {

    private lateinit var binding: FragmentCreateIdCardPluginBinding
    private val viewModel: IdCardPluginViewModel by viewModels()
    private var imagePath: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateIdCardPluginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        binding.btnSubmit.setOnClickListener(this)
        binding.tvIconSelection.setOnClickListener(this)
        binding.userImage.setOnClickListener(this)
        with(requireActivity() as MainActivity){
            binding.appBar.btnBack.setOnClickListener(this@IdCardPluginFragment)
            binding.ivBtHome.gone()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.userImage,R.id.tvIconSelection -> {
                PermissionX.init(mainActivity)
                    .permissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    .request { allGranted, grantedList, deniedList ->
                        pickFromGallery()
                    }
            }
            R.id.btnSubmit -> {
                viewModel.createUserId(
                    name = binding.etName.text.toString(),
                    phone = binding.etPhone.text.toString(),
                    email = binding.etEmail.text.toString(),
                    address = binding.etAddress.text.toString(),
                    image = imagePath
                )
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDataValidation(pluginDetails: HashMap<String, Any>) {
        replaceFragment(CardCreationFragment(), addToBackStack = false, bundle = Bundle().also {
            it.putSerializable(PLUGIN_MODEL, pluginDetails)
        })
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    var selection: String = ""
    private fun pickFromGallery(selectionType: String = IS_IMAGE_VIDEO) {
        selection = selectionType
        Intent(Intent.ACTION_GET_CONTENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            when (selectionType) {
                IS_IMAGE_VIDEO -> putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
            }
            selectionFromGalleryResult.launch(this)
        }
    }

    private val selectionFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val filePath =
                    result?.data?.data?.let { FileUriUtils.getRealPath(mainActivity, it) } ?: ""
                val file = File(filePath)
                binding.rlProfile.visible()

                viewModel.uploadImage(Uri.fromFile(file)) {
                    binding.userImage.loadImage(it)
                    imagePath = it
                }
            }
        }
}