package com.app.onlive.view.fragments.carddetails

import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.CardState
import com.app.onlive.enums.Plugins
import com.app.onlive.models.response.*
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.utils.Constants

class CardInHandDetailViewModel :
    BaseViewModel<CardInHandDetailViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun createCard(cardDetails: CardsResponse, state: CardState) {
        val groupId = prefManager.getStringPref(
            Constants.EXTRA_GROUP_ID,
            null
        ).toString()

        if (state == CardState.PUBLISHED && prefManager.getStringPref(
                Constants.EXTRA_GROUP_ID,
                null
            ).isNullOrBlank()
        ) {
            getViewNavigator()?.onDetailsUpdateError("Sorry, you need to join a group to publish the card!")
            return
        }

        if (cardDetails.cardId.isNullOrBlank().not() && cardDetails.isCardShareAble.not()) {
            getViewNavigator()?.onDetailsUpdateError("This card is in read-only mode")
            return
        }

        //todo: make an array that will contain the details of the saved card users
        // and with the help of that array make logic to stop users from republishing card

        getViewNavigator()?.showProgressBar()

        val hashMap = HashMap<String, Any>()
        hashMap["createdBy"] = cardDetails.createdBy.toString()
        hashMap["cardTitle"] = cardDetails.cardTitle.toString()
        hashMap["cardIcon"] = cardDetails.cardIcon.toString()
        hashMap["isCardShareAble"] = cardDetails.isCardShareAble
        hashMap["cardState"] = state.title
        hashMap["cardGroupId"] = groupId
        hashMap["cardPluginId"] = cardDetails.cardPluginId.toString()
        hashMap["cardPluginIcon"] = cardDetails.cardPluginIcon.toString()
        hashMap["cardCreatorId"] =
            getAppRepository().getUserAuth().uid.toString()

        when (cardDetails.cardPluginId) {
            Plugins.ONLIVEID.id -> {
                hashMap["idCardName"] = cardDetails.idCardResponse?.idCardName.toString()
                hashMap["idCardPhone"] = cardDetails.idCardResponse?.idCardPhone.toString()
                hashMap["idCardEmail"] = cardDetails.idCardResponse?.idCardEmail.toString()
                hashMap["idCardAddress"] = cardDetails.idCardResponse?.idCardAddress.toString()
                hashMap["idCardImage"] = cardDetails.idCardResponse?.idCardImage.toString()
            }
            Plugins.DOCUMENT.id -> {
                hashMap["documentDetails"] =
                    cardDetails.documentCardResponse?.documentDetails.toString()
                hashMap["documentPathURL"] =
                    cardDetails.documentCardResponse?.documentPathURL.toString()
                hashMap["documentExtensionType"] =
                    cardDetails.documentCardResponse?.documentExtensionType.toString()
            }
            Plugins.QUESTIONNAIRE.id -> {
                val questionerCardData = cardDetails.questionerCardResponse!!

                hashMap["questionerQuestion"] = questionerCardData.questionerQuestion.toString()
                hashMap["questionerOption1"] =
                    hashMapOf(questionerCardData.questionerOption1?.value to questionerCardData.questionerOption1?.count)
                hashMap["questionerOption2"] =
                    hashMapOf(questionerCardData.questionerOption2?.value to questionerCardData.questionerOption2?.count)
                hashMap["questionerOption3"] =
                    hashMapOf(questionerCardData.questionerOption3?.value to questionerCardData.questionerOption3?.count)
                hashMap["questionerOption4"] =
                    hashMapOf(questionerCardData.questionerOption4?.value to questionerCardData.questionerOption4?.count)
            }
            else -> {

            }
        }

        getAppRepository().saveDataInFireBase(
            DOCUMENT_PATH_CARDS,
            hashMap
        ) { documentReference, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank().not()) {
                getViewNavigator()?.onDetailsUpdateError(error!!)
            } else {
                getViewNavigator()?.onCardCreation(
                    "Card ${state.title.lowercase()} successfully",
                    documentReference?.id.toString()
                )
            }
        }
    }

    private var idCardResponse: IdCardResponse = IdCardResponse()
    private var documentCardResponse: DocumentCardResponse = DocumentCardResponse()
    private var questionerCardResponse: QuestionerCardResponse = QuestionerCardResponse()

    fun getCardDetails(documentId: String) {
        getViewNavigator()?.showProgressBar()
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PATH_CARDS,
            documentId
        ) { document, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank()) {
                document?.data?.let {
                    when (it["cardPluginId"].toString()) {
                        Plugins.ONLIVEID.id -> {
                            idCardResponse.apply {
                                idCardName = it["idCardName"].toString()
                                idCardPhone = it["idCardPhone"].toString()
                                idCardEmail = it["idCardEmail"].toString()
                                idCardAddress = it["idCardAddress"].toString()
                                idCardImage = it["idCardImage"].toString()
                            }
                        }
                        Plugins.DOCUMENT.id -> {
                            documentCardResponse.apply {
                                documentDetails = it["documentDetails"].toString()
                                documentPathURL = it["documentPathURL"].toString()
                                documentExtensionType = it["documentExtensionType"].toString()
                            }
                        }
                        Plugins.QUESTIONNAIRE.id -> {
                            questionerCardResponse.apply {
                                questionerQuestion = it["questionerCardResponse"].toString()
                                questionerOption1 = getQuestionerCardOptions(
                                    key = "questionerOption1",
                                    hashMap = it["questionerOption1"] as HashMap<*, *>
                                )
                                questionerOption2 = getQuestionerCardOptions(
                                    key = "questionerOption2",
                                    hashMap = it["questionerOption2"] as HashMap<*, *>
                                )
                                questionerOption3 = getQuestionerCardOptions(
                                    key = "questionerOption3",
                                    hashMap = it["questionerOption3"] as HashMap<*, *>
                                )
                                questionerOption4 = getQuestionerCardOptions(
                                    key = "questionerOption4",
                                    hashMap = it["questionerOption4"] as HashMap<*, *>
                                )
                            }
                        }
                    }

                    val card = CardsResponse(
                        cardId = document.id,
                        createdBy = it["createdBy"].toString(),
                        cardTitle = it["cardTitle"].toString(),
                        cardIcon = it["cardIcon"].toString(),
                        isCardShareAble = it["isCardShareAble"] as Boolean,
                        cardState = it["cardState"].toString(),
                        cardGroupId = it["cardGroupId"].toString(),
                        cardPluginId = it["cardPluginId"].toString(),
                        cardPluginIcon = it["cardPluginIcon"].toString(),
                        cardCreatorId = it["cardCreatorId"].toString(),
                        idCardResponse = idCardResponse,
                        documentCardResponse = documentCardResponse,
                        questionerCardResponse = questionerCardResponse
                    )
                    getViewNavigator()?.onCardDetails(card)
                }
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    fun onCardCancelOrDeletion(cardDetails: CardsResponse) {
        if (cardDetails.cardId.isNullOrBlank().not()
            && cardDetails.cardCreatorId == getAppRepository().getUserAuth().uid
        ) {
            setAppRepository().deleteDocumentById(
                DOCUMENT_PATH_CARDS,
                cardDetails.cardId!!
            ) { isDeleted, message ->
                if (isDeleted) {
                    getViewNavigator()?.onCardDeletion()
                    getViewNavigator()?.onDetailsUpdateError(message)
                } else
                    getViewNavigator()?.onDetailsUpdateError("Something went wrong!")
            }
        } else {
            getViewNavigator()?.onCardDeletion()
        }
    }

    fun updateDeleteButtonText(cardDetails: CardsResponse, text: (String) -> Unit) {
        if (cardDetails.cardId.isNullOrBlank().not()
            && cardDetails.cardCreatorId == getAppRepository().getUserAuth().uid
        ) {
            text.invoke("Delete")
        } else
            text.invoke("Cancel")
    }


    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onCardCreation(message: String, documentId: String)
        fun onCardDetails(cardDetails: CardsResponse)
        fun onDetailsUpdateError(message: String)
        fun onCardDeletion()
    }

    companion object {
        private const val DOCUMENT_PATH_CARDS = "cards"

        fun getQuestionerCardOptions(hashMap: HashMap<*, *>, key: String): QuestionerCardOptions {
            return QuestionerCardOptions(
                key = key,
                value = hashMap.keys.toMutableList()[0] as String,
                count = hashMap.values.toMutableList()[0] as Long
            )
        }
    }
}