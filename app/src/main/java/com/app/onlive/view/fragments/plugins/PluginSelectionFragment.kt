package com.app.onlive.view.fragments.plugins

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.adapter.PluginSelectionAdapter
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentPluginListBinding
import com.app.onlive.enums.Plugins
import com.app.onlive.extensions.*
import com.app.onlive.models.response.PluginResponse
import com.app.onlive.view.activities.MainActivity
import com.app.onlive.view.fragments.plugins.documentplugin.DocumentPluginFragment
import com.app.onlive.view.fragments.plugins.idcard.IdCardPluginFragment
import com.app.onlive.view.fragments.plugins.questionerplugin.QuestionerPluginFragment
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

class PluginSelectionFragment(
    private val multiSelection: Boolean = true,
    private var allowedPluginList: ArrayList<String>? = null,
) : BaseFragment(), View.OnClickListener, PluginSelectionAdapter.OnPluginSelection,
    PluginViewModel.View {

    private lateinit var binding: FragmentPluginListBinding
    private val pluginHashMap = HashMap<String, PluginResponse>()
    private var pluginSelectionAdapter: PluginSelectionAdapter? = null
    private val viewModel: PluginViewModel by viewModels()
    //private var groupId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPluginListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.let {
            it.attachView(this)
            it.getAllAvailablePlugins(allowedPluginList)
        }

        //groupId = prefManager.getStringPref(Constants.EXTRA_GROUP_ID, null)

        selectedPluginList.clear()

        pluginSelectionAdapter = PluginSelectionAdapter(this, multiSelection)
        binding.rvPlugin.adapter = pluginSelectionAdapter
        with(requireActivity() as MainActivity){
            binding.appBar.btnBack.setOnClickListener(this@PluginSelectionFragment)
            binding.ivBtHome.gone()
        }

        if (multiSelection) {
            binding.btnSubmit.visible()
            binding.btnSubmit.setOnClickListener(this)
        } else {
            binding.btnSubmit.gone()
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity()) {
            mainActivity.backPress()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.btnSubmit -> {
                val mapList = ArrayList(pluginHashMap.values)
                mapList.forEach {
                    selectedPluginList.add(it.id!!)
                }
                mainActivity.backPress()
            }
        }
    }

    override fun onItemCheckedListener(datum: PluginResponse) {
        pluginHashMap[datum.id ?: datum.title!!] = datum
    }

    override fun onItemUnCheckedListener(datum: PluginResponse) {
        pluginHashMap.remove(datum.id ?: datum.title!!)
    }

    override fun onItemSelectionListener(datum: PluginResponse) {
        selectedPluginList.clear()
        selectedPluginList.add(datum.id!!)

        when (datum.id) {
            Plugins.ONLIVEID.id -> {
                replaceFragment(
                    IdCardPluginFragment()
                )
            }
            Plugins.DOCUMENT.id -> {
                replaceFragment(
                    DocumentPluginFragment()
                )
            }
            Plugins.QUESTIONNAIRE.id -> {
                replaceFragment(
                    QuestionerPluginFragment()
                )
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onFindingPlugins(plugins: List<PluginResponse>) {
        pluginSelectionAdapter?.setItems(plugins)
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    companion object {
        var selectedPluginList = ArrayList<String>()
    }
}