package com.app.onlive.view.fragments.groupqrscanner

import com.app.onlive.base.BaseViewModel
import com.app.onlive.repository.PlatformRepository

class QRScannerGroupJoinerViewModel :
    BaseViewModel<QRScannerGroupJoinerViewModel.View, PlatformRepository>() {

    override fun setAppRepository() = PlatformRepository.getInstance()

    fun checkIfGroupAvailable(qrCode: String) {
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PATH,
            selectedId = qrCode
        ) { _, error ->
            if (error.isNullOrBlank()) {
                getViewNavigator()?.onFindingGroup(qrCode)
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    interface View {
        fun onFindingGroup(groupId: String)
        fun showProgressBar()
        fun dismissProgressBar()
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH = "groups"
    }
}