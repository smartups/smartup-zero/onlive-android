package com.app.onlive.view.fragments.plugins.showplugins.showdocument

import com.app.onlive.base.BaseViewModel
import com.app.onlive.models.response.DocumentCardResponse
import com.app.onlive.repository.PlatformRepository

class ShowDocumentCardViewModel :
    BaseViewModel<ShowDocumentCardViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getCardDetails(documentId: String) {
        getViewNavigator()?.showProgressBar()
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PATH_CARDS,
            documentId
        ) { document, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank()) {
                document?.let {
                    val card = DocumentCardResponse(
                        documentDetails = it.data?.get("documentDetails").toString(),
                        documentPathURL = it.data?.get("documentPathURL").toString(),
                        documentExtensionType = it.data?.get("documentExtensionType").toString(),
                    )
                    getViewNavigator()?.onCardDetails(card)
                }
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onCardDetails(documentCardResponse: DocumentCardResponse)
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH_CARDS = "cards"
    }
}