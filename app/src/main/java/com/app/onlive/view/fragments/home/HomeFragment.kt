package com.app.onlive.view.fragments.home

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.adapter.HomeAdapter
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentHomeBinding
import com.app.onlive.extensions.*
import com.app.onlive.models.response.GroupsResponse
import com.app.onlive.utils.Constants.EXTRA_GROUP_ID
import com.app.onlive.utils.Constants.USER_PROFILE
import com.app.onlive.utils.LocationPermissions
import com.app.onlive.utils.UtilityFunctions.isPermissionsGiven
import com.app.onlive.view.activities.LoginActivity
import com.app.onlive.view.fragments.card.CardsStageFragment
import com.app.onlive.view.fragments.group.GroupCreationFragment
import com.app.onlive.view.fragments.plugins.PluginSelectionFragment
import com.app.onlive.view.fragments.groupqrscanner.QRScannerGroupJoinerFragment
import com.app.onlive.view.fragments.savedcard.SavedCardsFragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng

class HomeFragment : BaseFragment(), HomeViewModel.View, View.OnClickListener,
    HomeAdapter.SetOnClickHandler, LocationPermissions.LocationPermissionCallBack {

    private val viewModel: HomeViewModel by viewModels()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var binding: FragmentHomeBinding
    private lateinit var homeAdapter: HomeAdapter
    private var userLocation: LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mainActivity)

        with(viewModel) {
            attachView(this@HomeFragment)
           // loadGroupFirstTime()
        }

        initOnLocationChangeListener {
            userLocation = LatLng(it.latitude, it.longitude)
        }

        PluginSelectionFragment.selectedPluginList.clear()
        homeAdapter = HomeAdapter(setOnClickHandler = this)
        binding.groupListRecyclerView.adapter = homeAdapter

        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@HomeFragment)
            binding.appBar.imgSavedCards.setOnClickListener(this@HomeFragment)
            binding.ivBtHome.setOnClickListener(this@HomeFragment)
            binding.ivBtHome.visible()

            binding.appBar.userProfileImage.loadImage(
                prefManager.getStringPref(
                    USER_PROFILE, null
                )
            )
            onBackPressedDispatcher.addCallback(this) {
                onDoubleBackPressed()
            }
        }

        binding.btnRefresh.setOnClickListener(this)

        // Configure the refreshing colors
        binding.layoutSwiper.apply {
            setOnRefreshListener {
                loadAvailableGroups()
            }
            setColorSchemeResources(
                R.color.primary,
                R.color.yellow,
                R.color.green_dark
            )
        }
    }

    @SuppressLint("MissingPermission")
    private fun loadGroupFirstTime() {
        isPermissionsGiven(mainActivity) {
            if (it)
                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        location?.run {
                            userLocation = LatLng(latitude, longitude)
                            loadAvailableGroups()
                        }
                    }
            else {
                binding.btnRefresh.visible()
                showToastMsg("Location permissions required")
            }
        }
    }

    private fun loadAvailableGroups() {
        userLocation?.let {
            viewModel.getAllAvailableGroups(it)
        }
    }

    override fun onFindingGroups(groups: List<GroupsResponse>) {
        binding.btnRefresh.gone()
        binding.layoutSwiper.isRefreshing = false
        homeAdapter.setItems(groups)
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDetailsUpdateError(message: String) {
        binding.btnRefresh.visible()
        showToastMsg(message)
    }

    override fun onClickHandler(group: GroupsResponse) {
        prefManager.putStringPref(EXTRA_GROUP_ID, group.id)
        replaceFragment(CardsStageFragment(), false)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_bt_home -> {
                mainActivity.showOptionsSelectionDialogue(
                    arrayOf(
                        "Create group",
                        "Scan to join the group",
                        "Logout"
                    )
                ) {
                    when (it) {
                        0 -> {
                            replaceFragment(GroupCreationFragment(), addToBackStack = true)
                        }
                        1 -> {
                            replaceFragment(QRScannerGroupJoinerFragment(), addToBackStack = true)
                        }
                        else -> {
                            prefManager.logoutSession()
                            startActivity(Intent(mainActivity, LoginActivity::class.java))
                            mainActivity.finish()
                        }
                    }
                }

            }
            R.id.btnRefresh -> loadAvailableGroups()
            R.id.btn_back -> mainActivity.onDoubleBackPressed()
            R.id.imgSavedCards -> mainActivity.replaceFragmentSafely(
                SavedCardsFragment(),
                addToBackStack = true
            )
        }
    }

    @SuppressLint("MissingPermission")
    private fun initOnLocationChangeListener(fusedLocation: ((Location) -> Unit)?) {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    fusedLocation?.invoke(location)
                }
            }
        }

        val locationRequest = LocationRequest.create().apply {
            interval = 15000
            fastestInterval = 5000
            smallestDisplacement = 10f
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        isPermissionsGiven(mainActivity) {
            if (it) {
                fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.getMainLooper()
                )
            } else {
                showToastMsg("Location permissions required")
                return@isPermissionsGiven
            }
        }
    }

    override fun onLocationPermissionChangeListener(permission: Boolean) {

    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        super.onResume()
        loadGroupFirstTime()
    }
}