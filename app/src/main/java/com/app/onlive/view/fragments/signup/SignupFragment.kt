package com.app.onlive.view.fragments.signup

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseLoginFragment
import com.app.onlive.databinding.FragmentSignupBinding
import com.app.onlive.extensions.loadImage
import com.app.onlive.extensions.replaceFragment
import com.app.onlive.extensions.showToastMsg
import com.app.onlive.utils.Constants
import com.app.onlive.utils.FileUriUtils
import com.app.onlive.view.fragments.login.LoginFragment
import com.google.firebase.auth.FirebaseUser
import com.permissionx.guolindev.PermissionX
import java.io.File

class SignupFragment : BaseLoginFragment(), SignupViewModel.View, View.OnClickListener {

    private val viewModel: SignupViewModel by viewModels()
    private lateinit var binding: FragmentSignupBinding
    private var imagePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            attachView(this@SignupFragment)
        }

        binding.btnLogin.setOnClickListener {
            replaceFragment(LoginFragment(), addToBackStack = false)
        }

        binding.layoutUserImage.setOnClickListener(this)
        binding.btConfirm.setOnClickListener(this)
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onSignupSuccess(firebaseUser: FirebaseUser, message: String) {
        viewModel.createProfile(imagePath, binding.etEmail.text.toString())
    }

    override fun onProfileCreation(message: String) {
        showToastMsg(message)
        replaceFragment(LoginFragment(), addToBackStack = false)
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.layout_userImage -> {
                PermissionX.init(loginActivity)
                    .permissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    .request { allGranted, grantedList, deniedList ->
                        pickFromGallery()
                    }
            }
            R.id.bt_confirm -> {
                viewModel.signup(
                    loginActivity,
                    binding.etEmail.text.toString(),
                    binding.etPassword.text.toString()
                )
            }
        }
    }

    var selection: String = ""
    private fun pickFromGallery(selectionType: String = Constants.IS_IMAGE_VIDEO) {
        selection = selectionType
        Intent(Intent.ACTION_GET_CONTENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            when (selectionType) {
                Constants.IS_IMAGE_VIDEO -> putExtra(
                    Intent.EXTRA_MIME_TYPES,
                    arrayOf("image/*")
                )
            }
            selectionFromGalleryResult.launch(this)
        }
    }

    private val selectionFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val filePath =
                    result?.data?.data?.let { FileUriUtils.getRealPath(loginActivity, it) }
                        ?: ""
                val file = File(filePath)
                viewModel.uploadImage(Uri.fromFile(file)){
                imagePath = it
                binding.civUserProfile.loadImage(it)
                }
            }
        }

}