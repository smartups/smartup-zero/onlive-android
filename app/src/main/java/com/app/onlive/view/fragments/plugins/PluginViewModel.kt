package com.app.onlive.view.fragments.plugins

import com.app.onlive.base.BaseViewModel
import com.app.onlive.models.response.PluginResponse
import com.app.onlive.repository.PlatformRepository

class PluginViewModel : BaseViewModel<PluginViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getAllAvailablePlugins(allowedPluginList: ArrayList<String>? = null) {
        setAppRepository().getDataFromFireBase(DOCUMENT_PATH) { querySnapshot, error ->
            if (error.isNullOrBlank()) {
                val plugins = ArrayList<PluginResponse>()
                querySnapshot?.forEach { document ->
                    if (allowedPluginList.isNullOrEmpty().not()) {
                        if (allowedPluginList?.contains(document.id) == true)
                            plugins.add(
                                PluginResponse(
                                    id = document.id,
                                    title = document.data["title"].toString(),
                                    details = document.data["details"].toString(),
                                    icon = document.data["icon"].toString(),
                                    createdBy = document.data["createdBy"].toString(),
                                    color = document.data["color"].toString(),
                                )
                            )
                    } else {
                        plugins.add(
                            PluginResponse(
                                id = document.id,
                                title = document.data["title"].toString(),
                                details = document.data["details"].toString(),
                                icon = document.data["icon"].toString(),
                                createdBy = document.data["createdBy"].toString(),
                                color = document.data["color"].toString(),
                            )
                        )
                    }
                }
                getViewNavigator()?.onFindingPlugins(plugins)
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onFindingPlugins(plugins: List<PluginResponse>)
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH = "plugins"
    }
}