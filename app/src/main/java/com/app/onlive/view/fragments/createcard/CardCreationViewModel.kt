package com.app.onlive.view.fragments.createcard

import android.net.Uri
import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.Plugins
import com.app.onlive.models.response.*
import com.app.onlive.repository.PlatformRepository

class CardCreationViewModel : BaseViewModel<CardCreationViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun uploadImage(uri: Uri, onSuccess: (imgPath: String) -> Unit) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().uploadFileToFireBase(uri) { imgPath, error ->
            getViewNavigator()?.dismissProgressBar()
            imgPath?.let {
                onSuccess.invoke(it)
            } ?: getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    private var idCardResponse: IdCardResponse? = null
    private var documentCardResponse: DocumentCardResponse? = null
    private var questionerCardResponse: QuestionerCardResponse? = null

    fun createCard(
        cardTitle: String?,
        createdBy: String?,
        cardIcon: String?,
        isShareable: Boolean,
        pluginHashMap: HashMap<String, Any>
    ) {
        if (cardTitle.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Card title is required")
            return
        }

        if (createdBy.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Created by is required")
            return
        }

        if (cardIcon.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Card icon is required")
            return
        }

        when (pluginHashMap["cardPluginId"].toString()) {
            Plugins.ONLIVEID.id -> {
                idCardResponse = IdCardResponse(
                    idCardName = pluginHashMap["idCardName"].toString(),
                    idCardPhone = pluginHashMap["idCardPhone"].toString(),
                    idCardEmail = pluginHashMap["idCardEmail"].toString(),
                    idCardAddress = pluginHashMap["idCardAddress"].toString(),
                    idCardImage = pluginHashMap["idCardImage"].toString(),
                )
            }
            Plugins.DOCUMENT.id -> {
                documentCardResponse = DocumentCardResponse(
                    documentDetails = pluginHashMap["documentDetails"].toString(),
                    documentPathURL = pluginHashMap["documentPathURL"].toString(),
                    documentExtensionType = pluginHashMap["documentExtensionType"].toString(),
                )
            }
            Plugins.QUESTIONNAIRE.id -> {
                questionerCardResponse = QuestionerCardResponse(
                    questionerQuestion = pluginHashMap["questionerQuestion"].toString(),
                    questionerOption1 = QuestionerCardOptions(
                        key = "questionerOption1",
                        value = pluginHashMap["questionerOption1"].toString(),
                        count = 0
                    ),
                    questionerOption2 = QuestionerCardOptions(
                        key = "questionerOption2",
                        value = pluginHashMap["questionerOption2"].toString(),
                        count = 0
                    ),
                    questionerOption3 = QuestionerCardOptions(
                        key = "questionerOption3",
                        value = pluginHashMap["questionerOption3"].toString(),
                        count = 0
                    ),
                    questionerOption4 = QuestionerCardOptions(
                        key = "questionerOption4",
                        value = pluginHashMap["questionerOption4"].toString(),
                        count = 0
                    )
                )
            }
        }

        val card = CardsResponse(
            //details from card details screen
            createdBy = createdBy,
            cardTitle = cardTitle,
            cardIcon = cardIcon,
            isCardShareAble = isShareable,

            //details from hashmap/plugin screen
            cardState = pluginHashMap["cardState"].toString(),
            cardGroupId = pluginHashMap["cardGroupId"].toString(),
            cardPluginId = pluginHashMap["cardPluginId"].toString(),
            cardPluginIcon = pluginHashMap["cardPluginIcon"].toString(),
            cardCreatorId = pluginHashMap["cardCreatorId"].toString(),

            //details from hashmap/plugin screen
            idCardResponse = idCardResponse,
            documentCardResponse = documentCardResponse,
            questionerCardResponse = questionerCardResponse
        )

        getViewNavigator()?.onAddingCardDetails(card)
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onAddingCardDetails(card: CardsResponse)
        fun onDetailsUpdateError(message: String)
    }
}