package com.app.onlive.view.fragments.maps

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentMapPinBinding
import com.app.onlive.extensions.backPress
import com.app.onlive.extensions.showToastMsg
import com.app.onlive.utils.UtilityFunctions.isPermissionsGiven
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.maps.route.extensions.drawMarker
import com.maps.route.extensions.moveCameraOnMap

class SetMapPinFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback {

    private var googleMap: GoogleMap? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var binding: FragmentMapPinBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapPinBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedLatLng = LatLng(52.309910816904626, 4.764167329524372)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mainActivity)
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.btnSetPin.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnSetPin -> {
                if (selectedLatLng == LatLng(52.309910816904626, 4.764167329524372)){
                    showToastMsg("Please set location")
                }else{
                    geoFenceRadius = binding.sliderRange.value.toDouble()
                    mainActivity.backPress()
                    showToastMsg(binding.sliderRange.value.toString())
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0
        googleMap?.run {
            isPermissionsGiven(mainActivity) {
                if (it)
                    fusedLocationProviderClient.lastLocation
                        .addOnSuccessListener { location: Location? ->
                            location?.run {
                                moveCameraOnMap(latLng = LatLng(latitude, longitude))
                            }
                        }
                else
                    showToastMsg("Location permissions required")
            }
            moveCameraOnMap(latLng = selectedLatLng!!)

            setOnMapClickListener {
                onLocationSelection(it)
            }

            isMyLocationEnabled = true
            uiSettings.isMyLocationButtonEnabled = true

        } ?: run {
            showToastMsg("Map isn't initialised")
        }
        mainActivity.invalidateOptionsMenu()
    }

    private fun onLocationSelection(latLng: LatLng) {
        googleMap?.run {
            clear()
            selectedLatLng = latLng
            moveCameraOnMap(latLng = latLng)
            drawMarker(latLng, mainActivity)
        }
    }

    companion object {
        var selectedLatLng: LatLng? = null
        var geoFenceRadius: Double = 0.025
    }
}
