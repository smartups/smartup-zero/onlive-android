package com.app.onlive.view.fragments.createcard

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCreateCardBinding
import com.app.onlive.extensions.*
import com.app.onlive.models.response.CardsResponse
import com.app.onlive.utils.Constants
import com.app.onlive.utils.Constants.CARD_MODEL
import com.app.onlive.utils.Constants.PLUGIN_MODEL
import com.app.onlive.utils.FileUriUtils
import com.app.onlive.view.fragments.carddetails.CardInHandDetailFragment
import com.permissionx.guolindev.PermissionX
import java.io.File

class CardCreationFragment : BaseFragment(),
    CardCreationViewModel.View, View.OnClickListener {

    private lateinit var binding: FragmentCreateCardBinding
    private val viewModel: CardCreationViewModel by viewModels()
    private var imagePath: String? = null
    private var pluginDetails: HashMap<String, Any>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateCardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        pluginDetails =
            arguments?.getSerializable(PLUGIN_MODEL) as? HashMap<String, Any>

        if (pluginDetails == null) {
            showToastMsg("Something went wrong")
            mainActivity.backPress()
            return
        }

        binding.tvChooseimage.setOnClickListener(this)
        binding.btCreateCard.setOnClickListener(this)

        with(mainActivity){
            binding.appBar.btnBack.setOnClickListener(this@CardCreationFragment)
            binding.ivBtHome.gone()
        }

        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.tv_chooseimage -> {
                PermissionX.init(mainActivity)
                    .permissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    .request { allGranted, grantedList, deniedList ->
                        pickFromGallery()
                    }
            }
            R.id.bt_createCard -> {
                pluginDetails ?: return

                viewModel.createCard(
                    cardTitle = binding.etDesc.text.toString(),
                    createdBy = binding.editText2.text.toString(),
                    cardIcon = imagePath,
                    isShareable = binding.cbAlowDownload.isChecked,
                    pluginHashMap = pluginDetails!!
                )
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onAddingCardDetails(card: CardsResponse) {
        //go to in hand state
        replaceFragment(CardInHandDetailFragment(), addToBackStack = false, bundle = Bundle().also {
            it.putParcelable(CARD_MODEL, card)
        })
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    var selection: String = ""
    private fun pickFromGallery(selectionType: String = Constants.IS_IMAGE_VIDEO) {
        selection = selectionType
        Intent(Intent.ACTION_GET_CONTENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            when (selectionType) {
                Constants.IS_IMAGE_VIDEO -> putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
            }
            selectionFromGalleryResult.launch(this)
        }
    }

    private val selectionFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val filePath =
                    result?.data?.data?.let { FileUriUtils.getRealPath(mainActivity, it) } ?: ""
                val file = File(filePath)

                viewModel.uploadImage(Uri.fromFile(file)) {
                    imagePath = it
                    binding.ivCard.loadImage(it)
                }
            }
        }
}