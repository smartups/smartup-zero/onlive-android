package com.app.onlive.view.fragments.plugins.showplugins.showidcard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentShowIdCardPluginBinding
import com.app.onlive.extensions.backPress
import com.app.onlive.extensions.showToastMsg
import android.content.Intent
import android.net.Uri
import androidx.activity.addCallback
import com.app.onlive.extensions.gone
import com.app.onlive.extensions.loadImage
import com.app.onlive.utils.Constants.CARD_ID
import com.app.onlive.view.activities.MainActivity

class ShowIdCardFragment : BaseFragment(), ShowIdCardViewModel.View,
    View.OnClickListener {

    private lateinit var binding: FragmentShowIdCardPluginBinding
    private val viewModel: ShowIdCardViewModel by viewModels()
    private var cardId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShowIdCardPluginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)
        cardId = arguments?.getString(CARD_ID)
        if (cardId == null) {
            showToastMsg("Something went wrong")
            mainActivity.backPress()
            return
        }

        viewModel.getCardDetails(cardId!!)

        binding.btnGoBack.setOnClickListener(this)
        binding.tvUserContact.setOnClickListener(this)
        binding.tvItemEmailAddress.setOnClickListener(this)

        with(mainActivity){
            binding.appBar.btnBack.setOnClickListener(this@ShowIdCardFragment)
            binding.ivBtHome.gone()
        }

        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back, R.id.btnGoBack -> mainActivity.backPress()
            R.id.tvUserContact -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${card?.userPhone}")
                startActivity(intent)
            }
            R.id.tvItemEmailAddress -> {
                val emailIntent = Intent(
                    Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", card?.userEmail, null
                    )
                )
                startActivity(Intent.createChooser(emailIntent, "Send email..."))
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    var card: ShowIdCardViewModel.IdCardResponse?= null
    override fun onCardDetails(idCardResponse: ShowIdCardViewModel.IdCardResponse) {
        card = idCardResponse
        with(binding) {
            tvItemUserName.text = idCardResponse.userName
            tvUserContact.text = idCardResponse.userPhone
            tvItemEmailAddress.text = idCardResponse.userEmail
            tvItemAddress.text = idCardResponse.userAddress
            imageUserprofileImage.loadImage(idCardResponse.userImage)
        }
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }
}