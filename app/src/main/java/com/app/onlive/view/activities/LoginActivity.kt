package com.app.onlive.view.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import com.app.onlive.base.BaseActivity
import com.app.onlive.databinding.ActivityLoginBinding
import com.app.onlive.extensions.replaceFragmentSafely
import com.app.onlive.utils.Constants
import com.app.onlive.view.fragments.login.LoginFragment
import com.app.onlive.view.fragments.splash.SplashFragment

class LoginActivity : BaseActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater).also {
            setContentView(it.root)
        }

        if (Constants.isLogoutCalled)
            replaceFragmentSafely(LoginFragment())
        else
            replaceFragmentSafely(SplashFragment())
    }
}
