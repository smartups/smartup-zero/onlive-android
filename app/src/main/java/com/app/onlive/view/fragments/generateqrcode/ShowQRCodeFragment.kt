package com.app.onlive.view.fragments.generateqrcode

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCreatedCardBinding
import com.app.onlive.databinding.FragmentShowQrcodeBinding
import com.app.onlive.extensions.backPress
import com.app.onlive.extensions.generateQRCode
import com.app.onlive.utils.Constants.QRCODE_INPUT

class ShowQRCodeFragment : BaseFragment() {

    private lateinit var binding: FragmentShowQrcodeBinding
    private var qrCodeInput: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShowQrcodeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        qrCodeInput = arguments?.getString(QRCODE_INPUT)
        val qrCode = qrCodeInput?.generateQRCode() ?: mainActivity.backPress()
        binding.imgViewQrCode.setImageBitmap(qrCode as Bitmap)

        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }

        mainActivity.binding.appBar.btnBack.setOnClickListener {
            mainActivity.backPress()
        }
    }
}