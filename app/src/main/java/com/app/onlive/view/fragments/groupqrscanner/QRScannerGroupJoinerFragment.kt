package com.app.onlive.view.fragments.groupqrscanner

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentQrscannerGroupJoinerBinding
import com.app.onlive.extensions.backPress
import com.app.onlive.extensions.replaceFragment
import com.app.onlive.extensions.showToastMsg
import com.app.onlive.extensions.visible
import com.app.onlive.utils.Constants
import com.app.onlive.view.fragments.card.CardsStageFragment
import com.budiyev.android.codescanner.*

class QRScannerGroupJoinerFragment : BaseFragment(), QRScannerGroupJoinerViewModel.View {

    private lateinit var binding: FragmentQrscannerGroupJoinerBinding
    private val viewModel: QRScannerGroupJoinerViewModel by viewModels()
    private lateinit var codeScanner: CodeScanner

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentQrscannerGroupJoinerBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.attachView(this)
        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }
        mainActivity.binding.ivBtHome.visible()

        codeScanner = CodeScanner(mainActivity, binding.scannerView)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            mainActivity.runOnUiThread {
                viewModel.checkIfGroupAvailable(it.text)
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            mainActivity.runOnUiThread {
                showToastMsg("Camera initialization error: ${it.message}")
            }
        }

        binding.scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun onFindingGroup(groupId: String) {
        prefManager.putStringPref(Constants.EXTRA_GROUP_ID, groupId)
        replaceFragment(CardsStageFragment(), false)
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }
}