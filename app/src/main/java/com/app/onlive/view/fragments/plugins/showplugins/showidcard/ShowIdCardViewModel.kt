package com.app.onlive.view.fragments.plugins.showplugins.showidcard

import com.app.onlive.base.BaseViewModel
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.view.fragments.carddetails.CardInHandDetailViewModel

class ShowIdCardViewModel : BaseViewModel<ShowIdCardViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getCardDetails(documentId: String) {
        getViewNavigator()?.showProgressBar()
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PATH_CARDS,
            documentId
        ) { document, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank()) {
                document?.let {
                    val card = IdCardResponse(
                        userName = it.data?.get("idCardName").toString(),
                        userPhone = it.data?.get("idCardPhone").toString(),
                        userEmail = it.data?.get("idCardEmail").toString(),
                        userAddress = it.data?.get("idCardAddress").toString(),
                        userImage = it.data?.get("idCardImage").toString(),
                    )
                    getViewNavigator()?.onCardDetails(card)
                }
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    data class IdCardResponse(
        var userName: String? = null,
        var userPhone: String? = null,
        var userEmail: String? = null,
        var userAddress: String? = null,
        var userImage: String? = null,
    )

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onCardDetails(idCardResponse: IdCardResponse)
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH_CARDS = "cards"
    }
}