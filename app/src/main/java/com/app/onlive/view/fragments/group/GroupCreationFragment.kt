package com.app.onlive.view.fragments.group

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentGroupBinding
import com.app.onlive.extensions.*
import com.app.onlive.utils.Constants.IS_IMAGE_VIDEO
import com.app.onlive.utils.FileUriUtils
import com.app.onlive.view.fragments.maps.SetMapPinFragment
import com.app.onlive.view.fragments.plugins.PluginSelectionFragment
import com.permissionx.guolindev.PermissionX
import java.io.File

class GroupCreationFragment : BaseFragment(), GroupViewModel.View, View.OnClickListener {

    private lateinit var binding: FragmentGroupBinding
    private val viewModel: GroupViewModel by viewModels()
    private var imagePath: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGroupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        binding.btnSubmit.setOnClickListener(this)
        binding.tvPluginSelection.setOnClickListener(this)
        binding.tvIconSelection.setOnClickListener(this)
        binding.rlProfile.setOnClickListener(this)
        binding.tvGroupAvailabilitySelection.setOnClickListener(this)

        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onGroupCreation(message: String) {
        showToastMsg(message)
        mainActivity.backPress()
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    var selection: String = ""
    private fun pickFromGallery(selectionType: String = IS_IMAGE_VIDEO) {
        selection = selectionType
        Intent(Intent.ACTION_GET_CONTENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            when (selectionType) {
                IS_IMAGE_VIDEO -> putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
            }
            selectionFromGalleryResult.launch(this)
        }
    }

    private val selectionFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val filePath =
                    result?.data?.data?.let { FileUriUtils.getRealPath(mainActivity, it) } ?: ""
                val file = File(filePath)
                binding.rlProfile.visible()

                viewModel.uploadImage(Uri.fromFile(file)) {
                    imagePath = it
                    binding.ivGroupPhoto.loadImage(it)
                }
            }
        }

    override fun onResume() {
        super.onResume()
        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@GroupCreationFragment)
            binding.appBar.imgSavedCards.setOnClickListener(this@GroupCreationFragment)
            binding.ivBtHome.gone()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.tvGroupAvailabilitySelection -> replaceFragment(
                SetMapPinFragment(),
                addToBackStack = true
            )
            R.id.tvPluginSelection -> replaceFragment(
                PluginSelectionFragment(multiSelection = true),
                addToBackStack = true
            )
            R.id.rl_profile, R.id.tvIconSelection -> {
                PermissionX.init(mainActivity)
                    .permissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    .request { allGranted, grantedList, deniedList ->
                        pickFromGallery()
                    }
            }
            R.id.btnSubmit -> {
                viewModel.createGroup(
                    binding.etTitle.text.toString(),
                    binding.etDetails.text.toString(),
                    imagePath,
                    PluginSelectionFragment.selectedPluginList,
                    SetMapPinFragment.selectedLatLng,
                    SetMapPinFragment.geoFenceRadius,
                    binding.simpleSwitch.isChecked
                )
            }
        }
    }
}