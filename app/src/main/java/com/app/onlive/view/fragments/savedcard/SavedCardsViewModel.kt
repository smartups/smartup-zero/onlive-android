package com.app.onlive.view.fragments.savedcard

import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.CardState
import com.app.onlive.enums.Plugins
import com.app.onlive.models.response.*
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.view.fragments.carddetails.CardInHandDetailViewModel

class SavedCardsViewModel : BaseViewModel<SavedCardsViewModel.View, PlatformRepository>() {

    private var idCardResponse: IdCardResponse = IdCardResponse()
    private var documentCardResponse: DocumentCardResponse = DocumentCardResponse()
    private var questionerCardResponse: QuestionerCardResponse = QuestionerCardResponse()

    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getAllSavedCards() {
        getAppRepository().getDataFromFireBase(
            DOCUMENT_PATH_CARDS,
            searchKey = CREATED_BY,
            searchValue = getAppRepository().getUserAuth().uid
        ) { querySnapshot, error ->
            if (error.isNullOrBlank()) {
                val cards = ArrayList<CardsResponse>()
                querySnapshot?.forEach {

                    if (it.data["cardState"].toString() == CardState.SAVED.name
                        && it.data["cardCreatorId"].toString() == getAppRepository().getUserAuth().uid
                    ) {

                        when (it.data["cardPluginId"].toString()) {
                            Plugins.ONLIVEID.id -> {
                                idCardResponse.apply {
                                    idCardName = it.data["idCardName"].toString()
                                    idCardPhone = it.data["idCardPhone"].toString()
                                    idCardEmail = it.data["idCardEmail"].toString()
                                    idCardAddress = it.data["idCardAddress"].toString()
                                    idCardImage = it.data["idCardImage"].toString()
                                }
                            }
                            Plugins.DOCUMENT.id -> {
                                documentCardResponse.apply {
                                    documentDetails = it.data["documentDetails"].toString()
                                    documentPathURL = it.data["documentPathURL"].toString()
                                    documentExtensionType =
                                        it.data["documentExtensionType"].toString()
                                }
                            }
                            Plugins.QUESTIONNAIRE.id -> {
                                questionerCardResponse.apply {
                                    questionerQuestion =
                                        it.data["questionerCardResponse"].toString()

                                    questionerOption1 =
                                        CardInHandDetailViewModel.getQuestionerCardOptions(
                                            key = "questionerOption1",
                                            hashMap = it.data["questionerOption1"] as HashMap<*, *>
                                        )
                                    questionerOption2 =
                                        CardInHandDetailViewModel.getQuestionerCardOptions(
                                            key = "questionerOption2",
                                            hashMap = it.data["questionerOption2"] as HashMap<*, *>
                                        )
                                    questionerOption3 =
                                        CardInHandDetailViewModel.getQuestionerCardOptions(
                                            key = "questionerOption3",
                                            hashMap = it.data["questionerOption3"] as HashMap<*, *>
                                        )
                                    questionerOption4 =
                                        CardInHandDetailViewModel.getQuestionerCardOptions(
                                            key = "questionerOption4",
                                            hashMap = it.data["questionerOption4"] as HashMap<*, *>
                                        )
                                }
                            }
                        }

                        cards.add(
                            CardsResponse(
                                cardId = it.id,
                                createdBy = it.data["createdBy"].toString(),
                                cardTitle = it.data["cardTitle"].toString(),
                                cardIcon = it.data["cardIcon"].toString(),
                                isCardShareAble = it.data["isCardShareAble"] as Boolean,
                                cardState = it.data["cardState"].toString(),
                                cardGroupId = it.data["cardGroupId"].toString(),
                                cardPluginId = it.data["cardPluginId"].toString(),
                                cardPluginIcon = it.data["cardPluginIcon"].toString(),
                                cardCreatorId = it.data["cardCreatorId"].toString(),
                                idCardResponse = idCardResponse,
                                documentCardResponse = documentCardResponse,
                                questionerCardResponse = questionerCardResponse
                            )
                        )
                    }
                }
                /*val cards = ArrayList<CardsResponse>()
                querySnapshot?.forEach { document ->
                    if (document.data["cardState"].toString() == CardState.SAVED.name) {
                        cards.add(
                            CardsResponse(
                                cardId = document.id,
                                createdBy = document.data["createdBy"].toString(),
                                cardTitle = document.data["cardTitle"].toString(),
                                cardIcon = document.data["cardIcon"].toString(),
                                isCardShareAble = document.data["isCardShareAble"] as Boolean,
                                cardState = document.data["cardState"].toString(),
                                cardGroupId = document.data["cardGroupId"].toString(),
                                cardPluginId = document.data["cardPluginId"].toString(),
                                cardPluginIcon = document.data["cardPluginIcon"].toString(),
                                cardCreatorId = document.data["cardCreatorId"].toString()
                            )
                        )
                    }
                }*/
                getViewNavigator()?.onFindingCards(cards)
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }


    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onFindingCards(cards: List<CardsResponse>)
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH_CARDS = "cards"
        private const val CREATED_BY = "cardCreatorId"
    }
}