package com.app.onlive.view.fragments.login

import com.app.onlive.base.BaseViewModel
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.utils.Constants.USER_EMAIL
import com.app.onlive.utils.Constants.USER_ID
import com.app.onlive.view.activities.LoginActivity
import com.app.onlive.utils.Constants.USER_PROFILE
import com.google.firebase.auth.FirebaseUser

class LoginViewModel : BaseViewModel<LoginViewModel.View, PlatformRepository>() {

    override fun setAppRepository() = PlatformRepository.getInstance()

    fun login(loginActivity: LoginActivity, email: String, password: String) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().signIn(loginActivity, email, password) { auth, error->
            getViewNavigator()?.dismissProgressBar()
            if (auth != null) {
                getViewNavigator()?.onLoginSuccess(auth, "Login successfully")
                prefManager.saveUserID(auth.uid)
                prefManager.saveSessionDetails(email, password)
            } else
                getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    fun getProfile() {
        getViewNavigator()?.showProgressBar()
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PROFILE_PATH,
            getAppRepository().getUserAuth().uid.toString()
        ) { document, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank()) {
                document?.data?.let {
                    prefManager.putStringPref(USER_PROFILE, it["userImage"].toString())
                    prefManager.putStringPref(USER_EMAIL, it["userEmail"].toString())
                    prefManager.putStringPref(USER_ID, it["userId"].toString())
                }
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onLoginSuccess(firebaseUser: FirebaseUser, message: String)
        fun onDetailsUpdateError(message: String)
    }

    companion object{
        private const val DOCUMENT_PROFILE_PATH = "profile"
    }
}