package com.app.onlive.view.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentTabLayoutBinding
import com.app.onlive.extensions.replaceFragmentInFragment

class BottomNavFragment : BaseFragment() {

    private lateinit var binding: FragmentTabLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentTabLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        replaceFragmentInFragment(binding.flFragment, HomeFragment())
        binding.bottomNavigationView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.allGroups -> {
                    replaceFragmentInFragment(binding.flFragment, HomeFragment(), true, null)
                }
                R.id.myGroups -> {
                    replaceFragmentInFragment(binding.flFragment, MyGroupsFragment(), true, null)
                }
            }
            true
        }
    }
}