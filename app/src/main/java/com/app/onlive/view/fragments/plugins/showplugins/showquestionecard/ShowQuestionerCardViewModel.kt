package com.app.onlive.view.fragments.plugins.showplugins.showquestionecard

import com.app.onlive.base.BaseViewModel
import com.app.onlive.models.response.QuestionerCardOptions
import com.app.onlive.models.response.QuestionerCardResponse
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.view.fragments.carddetails.CardInHandDetailViewModel

class ShowQuestionerCardViewModel :
    BaseViewModel<ShowQuestionerCardViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getCardDetails(documentId: String) {

        getViewNavigator()?.showProgressBar()
        setAppRepository().getDataFromFireBaseById(
            DOCUMENT_PATH_CARDS,
            documentId
        ) { document, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank()) {
                document?.let {

                    val optionMap1 = it.data?.get("questionerOption1") as HashMap<*, *>
                    val optionMap2 = it.data?.get("questionerOption2") as HashMap<*, *>
                    val optionMap3 = it.data?.get("questionerOption3") as HashMap<*, *>
                    val optionMap4 = it.data?.get("questionerOption4") as HashMap<*, *>

                    val questionerCardResponse = QuestionerCardResponse(
                        questionerQuestion = it.data?.get("questionerQuestion").toString(),

                        questionerOption1 = CardInHandDetailViewModel.getQuestionerCardOptions(
                            key = "questionerOption1",
                            hashMap = optionMap1
                        ),
                        questionerOption2 = CardInHandDetailViewModel.getQuestionerCardOptions(
                            key = "questionerOption2",
                            hashMap = optionMap2
                        ),
                        questionerOption3 = CardInHandDetailViewModel.getQuestionerCardOptions(
                            key = "questionerOption3",
                            hashMap = optionMap3
                        ),
                        questionerOption4 = CardInHandDetailViewModel.getQuestionerCardOptions(
                            key = "questionerOption4",
                            hashMap = optionMap4
                        )
                    )
                    getViewNavigator()?.onCardDetails(questionerCardResponse)
                }
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    fun castVoteForQuestioner(documentId: String, questionerOption: String, option: String, count: Long) {
        val hashMap = HashMap<String, Any>()
        hashMap[questionerOption] = hashMapOf(option to count + 1)

        getViewNavigator()?.showProgressBar()
        getAppRepository().updateDataInFireBase(
            collectionPath = DOCUMENT_PATH_CARDS,
            documentPath = documentId,
            hashMap = hashMap
        ) { isUpdated, message ->
            getViewNavigator()?.dismissProgressBar()
            if (isUpdated)
                getViewNavigator()?.onVoteSuccess("Voted successfully")
            else
                getViewNavigator()?.onDetailsUpdateError(message)
        }
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onCardDetails(questionerCardResponse: QuestionerCardResponse)
        fun onDetailsUpdateError(message: String)
        fun onVoteSuccess(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH_CARDS = "cards"
    }
}