package com.app.onlive.view.fragments.group

import android.net.Uri
import com.app.onlive.base.BaseViewModel
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.view.fragments.signup.SignupViewModel
import com.google.android.gms.maps.model.LatLng

class GroupViewModel : BaseViewModel<GroupViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun uploadImage(uri: Uri, onSuccess: (imgPath: String) -> Unit) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().uploadFileToFireBase(uri) { imgPath, error ->
            getViewNavigator()?.dismissProgressBar()
            imgPath?.let {
                onSuccess.invoke(it)
            } ?: getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    fun createGroup(
        groupTitle: String?,
        groupDetails: String?,
        groupIcon: String?,
        selectedPluginList: ArrayList<String>,
        latLng: LatLng?,
        geoFenceRadius: Double = 0.025,
        isPrivateGroup: Boolean = false
    ) {
        if (groupTitle.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Group title is required")
            return
        }

        if (groupDetails.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Group description is required")
            return
        }

        if (groupIcon.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Group icon is required")
            return
        }

        if (selectedPluginList.isEmpty()) {
            getViewNavigator()?.onDetailsUpdateError("You must select a plugin for your group")
            return
        }

        if (latLng == null) {
            getViewNavigator()?.onDetailsUpdateError("Group availability location is required")
            return
        }

        getViewNavigator()?.showProgressBar()

        val hashMap = HashMap<String, Any>()
        hashMap["title"] = groupTitle
        hashMap["details"] = groupDetails
        hashMap["icon"] = groupIcon
        hashMap["isOnlive"] = false
        hashMap["plugins"] = selectedPluginList
        hashMap["createdBy"] = getAppRepository().getUserAuth().uid.toString()
        hashMap["groupLatitude"] = latLng.latitude
        hashMap["groupLongitude"] = latLng.longitude
        hashMap["geoFenceRadius"] = geoFenceRadius
        hashMap["isPrivateGroup"] = isPrivateGroup

        getAppRepository().saveDataInFireBase(DOCUMENT_PATH, hashMap) { documentReference, error ->
            getViewNavigator()?.dismissProgressBar()
            if (error.isNullOrBlank().not())
                getViewNavigator()?.onDetailsUpdateError(error!!)
            else
                getViewNavigator()?.onGroupCreation("Group created successfully")
        }
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onGroupCreation(message: String)
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH = "groups"
    }
}