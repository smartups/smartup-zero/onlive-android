package com.app.onlive.view.fragments.plugins.documentplugin

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCreateDocumentPluginBinding
import com.app.onlive.enums.DocumentTypes
import com.app.onlive.extensions.*
import com.app.onlive.utils.Constants
import com.app.onlive.utils.Constants.IS_DOCUMENT
import com.app.onlive.utils.Constants.PLUGIN_MODEL
import com.app.onlive.utils.FileUriUtils
import com.app.onlive.view.activities.MainActivity
import com.app.onlive.view.fragments.createcard.CardCreationFragment
import com.permissionx.guolindev.PermissionX
import java.io.File

class DocumentPluginFragment : BaseFragment(), DocumentPluginViewModel.View,
    View.OnClickListener {

    private lateinit var binding: FragmentCreateDocumentPluginBinding
    private val viewModel: DocumentPluginViewModel by viewModels()
    private var documentPath: String? = null
    private var extension: String = DocumentTypes.WEB.title

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreateDocumentPluginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        binding.btnSubmit.setOnClickListener(this)
        with(requireActivity() as MainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@DocumentPluginFragment)
            binding.ivBtHome.gone()
        }
        binding.tvDocumentSelection.setOnClickListener(this)

        requireActivity().onBackPressedDispatcher.addCallback(requireActivity()) {
            mainActivity.backPress()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> mainActivity.backPress()
            R.id.tvDocumentSelection -> {
                PermissionX.init(mainActivity)
                    .permissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    .request { _, _, _ ->
                        mainActivity.showOptionsSelectionDialogue(
                            arrayOf("Document", "Photos")
                        ) {
                            when (it) {
                                0 -> {
                                    extension = DocumentTypes.PDF.title
                                    pickFromGallery(IS_DOCUMENT)
                                }
                                else -> {
                                    extension = DocumentTypes.IMAGE.title
                                    pickFromGallery(Constants.IS_IMAGE_VIDEO)
                                }
                            }
                        }
                    }
            }
            R.id.btnSubmit -> {
                val url = if (documentPath.isNullOrBlank()) {
                    extension = DocumentTypes.WEB.title
                    binding.etDocumentURl.text.toString()
                } else documentPath

                viewModel.createDocumentCard(
                    details = binding.etDetails.text.toString(),
                    documentUrl = url,
                    documentType = extension
                )
            }
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDataValidation(pluginDetails: HashMap<String, Any>) {
        replaceFragment(CardCreationFragment(), addToBackStack = false, bundle = Bundle().also {
            it.putSerializable(PLUGIN_MODEL, pluginDetails)
        })
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    private fun pickFromGallery(selectionType: String) {
        Intent(Intent.ACTION_GET_CONTENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "*/*"
            when (selectionType) {
                IS_DOCUMENT -> putExtra(
                    Intent.EXTRA_MIME_TYPES,
                    arrayOf("application/pdf", "text/plain")
                )
                Constants.IS_IMAGE_VIDEO -> putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
            }
            selectionFromGalleryResult.launch(this)
        }
    }

    private val selectionFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val filePath =
                    result?.data?.data?.let { FileUriUtils.getRealPath(mainActivity, it) } ?: ""
                val file = File(filePath)

                viewModel.uploadDocument(Uri.fromFile(file)) {
                    binding.etDocumentURl.setText(it)
                    documentPath = it
                }
            }
        }
}