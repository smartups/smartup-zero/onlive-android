package com.app.onlive.view.fragments.home

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.adapter.HomeAdapter
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentHomeBinding
import com.app.onlive.databinding.FragmentMyGroupsBinding
import com.app.onlive.extensions.*
import com.app.onlive.models.response.GroupsResponse
import com.app.onlive.utils.Constants
import com.app.onlive.utils.LocationPermissions
import com.app.onlive.utils.UtilityFunctions
import com.app.onlive.view.activities.LoginActivity
import com.app.onlive.view.fragments.card.CardsStageFragment
import com.app.onlive.view.fragments.group.GroupCreationFragment
import com.app.onlive.view.fragments.groupqrscanner.QRScannerGroupJoinerFragment
import com.app.onlive.view.fragments.plugins.PluginSelectionFragment
import com.app.onlive.view.fragments.savedcard.SavedCardsFragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng


class MyGroupsFragment : BaseFragment(), HomeViewModel.View, View.OnClickListener,
    HomeAdapter.SetOnClickHandler {

    private val viewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentMyGroupsBinding
    private lateinit var homeAdapter: HomeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMyGroupsBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            attachView(this@MyGroupsFragment)

        }

        PluginSelectionFragment.selectedPluginList.clear()
        homeAdapter = HomeAdapter(setOnClickHandler = this)
        binding.groupListRecyclerView.adapter = homeAdapter

        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@MyGroupsFragment)
            binding.appBar.imgSavedCards.setOnClickListener(this@MyGroupsFragment)
            binding.ivBtHome.setOnClickListener(this@MyGroupsFragment)
            binding.ivBtHome.visible()

            binding.appBar.userProfileImage.loadImage(
                prefManager.getStringPref(
                    Constants.USER_PROFILE, null
                )
            )
            onBackPressedDispatcher.addCallback(this) {
                onDoubleBackPressed()
            }
        }

        binding.btnRefresh.setOnClickListener(this)

        // Configure the refreshing colors
        binding.layoutSwiper.apply {
            setOnRefreshListener {
                loadAvailableGroups()
            }
            setColorSchemeResources(
                R.color.primary,
                R.color.yellow,
                R.color.green_dark
            )
        }
    }


    private fun loadAvailableGroups() {
        viewModel.getAllMyGroups()
    }

    override fun onFindingGroups(groups: List<GroupsResponse>) {
        binding.btnRefresh.gone()
        binding.layoutSwiper.isRefreshing = false
        homeAdapter.setItems(groups)
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDetailsUpdateError(message: String) {
        binding.btnRefresh.visible()
        showToastMsg(message)
    }

    override fun onClickHandler(group: GroupsResponse) {
        prefManager.putStringPref(Constants.EXTRA_GROUP_ID, group.id)
        replaceFragment(CardsStageFragment(), false)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_bt_home -> {
                mainActivity.showOptionsSelectionDialogue(
                    arrayOf(
                        "Create group",
                        "Scan to join the group",
                        "Logout"
                    )
                ) {
                    when (it) {
                        0 -> {
                            replaceFragment(GroupCreationFragment(), addToBackStack = true)
                        }
                        1 -> {
                            replaceFragment(QRScannerGroupJoinerFragment(), addToBackStack = true)
                        }
                        else -> {
                            prefManager.logoutSession()
                            startActivity(Intent(mainActivity, LoginActivity::class.java))
                            mainActivity.finish()
                        }
                    }
                }

            }
            R.id.btnRefresh -> loadAvailableGroups()
            R.id.btn_back -> mainActivity.onDoubleBackPressed()
            R.id.imgSavedCards -> mainActivity.replaceFragmentSafely(
                SavedCardsFragment(),
                addToBackStack = true
            )
        }
    }

    override fun onResume() {
        super.onResume()
        loadAvailableGroups()
    }
}