package com.app.onlive.view.fragments.home

import com.app.onlive.base.BaseViewModel
import com.app.onlive.models.response.GroupsResponse
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.utils.UtilityFunctions.isUserInRadius
import com.google.android.gms.maps.model.LatLng

class HomeViewModel : BaseViewModel<HomeViewModel.View, PlatformRepository>() {

    //private var indexOfOnLive = -1

    override fun setAppRepository() = PlatformRepository.getInstance()

    fun getAllAvailableGroups(userLocation: LatLng) {
        setAppRepository().getDataFromFireBase(DOCUMENT_PATH) { querySnapshot, error ->
            if (error.isNullOrBlank()) {

                val groups = ArrayList<GroupsResponse>()
                querySnapshot?.forEachIndexed { index, document ->

                    val latitude = document.data["groupLatitude"] as Double
                    val longitude = document.data["groupLongitude"] as Double
                    val radius = document.data["geoFenceRadius"].toString().toDouble()
                    var isPrivateGroup = document.data["isPrivateGroup"] as Boolean?
                    if (isPrivateGroup == null)
                        isPrivateGroup = false

                    if (isPrivateGroup.not() && isUserInRadius(
                            pointA = userLocation,
                            pointB = LatLng(latitude, longitude),
                            radius = radius
                        )
                    ) {
                        groups.add(
                            GroupsResponse(
                                id = document.id,
                                title = document.data["title"].toString(),
                                details = document.data["details"].toString(),
                                icon = document.data["icon"].toString(),
                                groupLatitude = latitude,
                                groupLongitude = longitude,
                                geoFenceRadius = radius
                            )
                        )
                    }
                }
                getViewNavigator()?.onFindingGroups(groups.asReversed())
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    fun getAllMyGroups() {
        setAppRepository().getDataFromFireBase(DOCUMENT_PATH) { querySnapshot, error ->
            if (error.isNullOrBlank()) {

                val groups = ArrayList<GroupsResponse>()
                querySnapshot?.forEachIndexed { index, document ->

                    val latitude = document.data["groupLatitude"] as Double
                    val longitude = document.data["groupLongitude"] as Double
                    val radius = document.data["geoFenceRadius"].toString().toDouble()
                    val createdBy = document.data["createdBy"] as String?

                    if (createdBy == getAppRepository().getUserAuth().uid) {
                        groups.add(
                            GroupsResponse(
                                id = document.id,
                                title = document.data["title"].toString(),
                                details = document.data["details"].toString(),
                                icon = document.data["icon"].toString(),
                                groupLatitude = latitude,
                                groupLongitude = longitude,
                                geoFenceRadius = radius
                            )
                        )
                    }
                }
                getViewNavigator()?.onFindingGroups(groups.asReversed())
            } else {
                getViewNavigator()?.onDetailsUpdateError(error)
            }
        }
    }

    interface View {
        fun onFindingGroups(groups: List<GroupsResponse>)
        fun showProgressBar()
        fun dismissProgressBar()
        fun onDetailsUpdateError(message: String)
    }

    companion object {
        private const val DOCUMENT_PATH = "groups"
    }
}