package com.app.onlive.view.fragments.plugins.questionerplugin

import android.net.Uri
import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.Plugins
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.utils.Constants

class QuestionerPluginViewModel :
    BaseViewModel<QuestionerPluginViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun createQuestionerCard(
        questionerQuestion: String?,
        questionerOption1: String?,
        questionerOption2: String?,
        questionerOption3: String?,
        questionerOption4: String?,
    ) {
        if (questionerQuestion.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("questioner is required")
            return
        }

        if (questionerOption1.isNullOrBlank() || questionerOption2.isNullOrBlank() || questionerOption3.isNullOrBlank() || questionerOption4.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Please provide all four options")
            return
        }

        val hashMap = HashMap<String, Any>()
        hashMap["questionerQuestion"] = questionerQuestion
        hashMap["questionerOption1"] = questionerOption1
        hashMap["questionerOption2"] = questionerOption2
        hashMap["questionerOption3"] = questionerOption3
        hashMap["questionerOption4"] = questionerOption4

        hashMap["cardPluginId"] = Plugins.QUESTIONNAIRE.id
        hashMap["cardPluginIcon"] = Plugins.QUESTIONNAIRE.pluginIcon
        hashMap["cardGroupId"] =
            prefManager.getStringPref(Constants.EXTRA_GROUP_ID, null).toString()
        hashMap["cardCreatorId"] = getAppRepository().getUserAuth().currentUser?.uid.toString()

        getViewNavigator()?.onDataValidation(hashMap)
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onDataValidation(pluginDetails: HashMap<String, Any>)
        fun onDetailsUpdateError(message: String)
    }
}