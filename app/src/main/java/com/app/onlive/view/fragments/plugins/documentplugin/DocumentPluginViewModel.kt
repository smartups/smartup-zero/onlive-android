package com.app.onlive.view.fragments.plugins.documentplugin

import android.net.Uri
import com.app.onlive.base.BaseViewModel
import com.app.onlive.enums.Plugins
import com.app.onlive.repository.PlatformRepository
import com.app.onlive.utils.Constants

class DocumentPluginViewModel : BaseViewModel<DocumentPluginViewModel.View, PlatformRepository>() {
    override fun setAppRepository() = PlatformRepository.getInstance()

    fun uploadDocument(uri: Uri, onSuccess: (documentPath: String) -> Unit) {
        getViewNavigator()?.showProgressBar()
        getAppRepository().uploadFileToFireBase(uri) { imgPath, error ->
            getViewNavigator()?.dismissProgressBar()
            imgPath?.let {
                onSuccess.invoke(it)
            } ?: getViewNavigator()?.onDetailsUpdateError(error!!)
        }
    }

    fun createDocumentCard(
        details: String?,
        documentUrl: String?,
        documentType: String?
    ) {
        if (details.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Document details are required")
            return
        }

        if (documentType.isNullOrBlank() || documentUrl.isNullOrBlank()) {
            getViewNavigator()?.onDetailsUpdateError("Couldn't find the attached document")
            return
        }

        val hashMap = HashMap<String, Any>()
        hashMap["documentDetails"] = details
        hashMap["documentPathURL"] = documentUrl
        hashMap["documentExtensionType"] = documentType

        hashMap["cardPluginId"] = Plugins.DOCUMENT.id
        hashMap["cardPluginIcon"] = Plugins.DOCUMENT.pluginIcon
        hashMap["cardGroupId"] =
            prefManager.getStringPref(Constants.EXTRA_GROUP_ID, null).toString()
        hashMap["cardCreatorId"] = getAppRepository().getUserAuth().currentUser?.uid.toString()
        hashMap["cardColor"] = Plugins.DOCUMENT.bgColor

        getViewNavigator()?.onDataValidation(hashMap)
    }

    interface View {
        fun showProgressBar()
        fun dismissProgressBar()
        fun onDataValidation(pluginDetails: HashMap<String, Any>)
        fun onDetailsUpdateError(message: String)
    }
}