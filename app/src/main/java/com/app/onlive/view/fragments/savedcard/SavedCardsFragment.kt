package com.app.onlive.view.fragments.savedcard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.adapter.CardsAdapter
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentCardsSavedBinding
import com.app.onlive.extensions.*
import com.app.onlive.models.response.CardsResponse
import com.app.onlive.utils.Constants.CARD_MODEL
import com.app.onlive.utils.Constants.EXTRA_GROUP_ID
import com.app.onlive.view.fragments.carddetails.CardInHandDetailFragment
import com.app.onlive.view.fragments.home.BottomNavFragment
import com.app.onlive.view.fragments.plugins.PluginSelectionFragment

class SavedCardsFragment : BaseFragment(), CardsAdapter.OnCardClickListener,
    SavedCardsViewModel.View {

    private lateinit var binding: FragmentCardsSavedBinding
    private val viewModel: SavedCardsViewModel by viewModels()
    private lateinit var cardsAdapter: CardsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCardsSavedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            attachView(this@SavedCardsFragment)
            getAllSavedCards()
        }

        PluginSelectionFragment.selectedPluginList.clear()
        cardsAdapter = CardsAdapter(onCardClickListener = this)
        binding.rvCards.adapter = cardsAdapter

        val groupId = prefManager.getStringPref(EXTRA_GROUP_ID, null)

        with(mainActivity) {
            binding.ivBtHome.gone()
            binding.appBar.btnBack.setOnClickListener {
                goBackToStack(groupId)
            }
            onBackPressedDispatcher.addCallback(this) {
                goBackToStack(groupId)
            }
        }
    }

    override fun clickListener(card: CardsResponse) {
        replaceFragment(CardInHandDetailFragment(), addToBackStack = true, bundle = Bundle().also {
            it.putParcelable(CARD_MODEL, card)
        })
    }

    private fun goBackToStack(groupId:String?){
        if (groupId.isNullOrBlank().not())
            leaveGroup()
        else
            replaceFragment(BottomNavFragment())
    }

    override fun onFindingCards(cards: List<CardsResponse>) {
        cardsAdapter.setItems(cards)
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }
}