package com.app.onlive.view.fragments.plugins.showplugins.showdocument

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentShowDocumnetCardPluginBinding
import com.app.onlive.enums.DocumentTypes
import com.app.onlive.models.response.DocumentCardResponse
import com.app.onlive.utils.Constants.CARD_ID
import com.rajat.pdfviewer.PdfViewerActivity
import androidx.browser.customtabs.CustomTabsIntent
import com.app.onlive.extensions.*
import com.app.onlive.view.activities.MainActivity
import saschpe.android.customtabs.CustomTabsHelper.Companion.addKeepAliveExtra
import saschpe.android.customtabs.WebViewFallback

import saschpe.android.customtabs.CustomTabsHelper
import saschpe.android.customtabs.CustomTabsHelper.Companion.openCustomTab


class ShowDocumentCardFragment : BaseFragment(), ShowDocumentCardViewModel.View,
    View.OnClickListener {

    private lateinit var binding: FragmentShowDocumnetCardPluginBinding
    private val viewModel: ShowDocumentCardViewModel by viewModels()
    private var cardId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShowDocumnetCardPluginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        cardId = arguments?.getString(CARD_ID)
        if (cardId == null) {
            showToastMsg("Something went wrong")
            mainActivity.backPress()
            return
        }

        viewModel.getCardDetails(cardId!!)

        binding.btnGoBack.setOnClickListener(this)
        with(mainActivity) {
            binding.appBar.btnBack.setOnClickListener(this@ShowDocumentCardFragment)
        }

        mainActivity.onBackPressedDispatcher.addCallback(mainActivity) {
            mainActivity.backPress()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back, R.id.btnGoBack -> mainActivity.backPress()
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onCardDetails(documentCardResponse: DocumentCardResponse) {
        binding.tvDocumentDetails.text = documentCardResponse.documentDetails
        when (documentCardResponse.documentExtensionType) {
            DocumentTypes.PDF.title -> {
                isDocumentAnImage(false)
                binding.imageDocumentPreview.setOnClickListener {
                    startActivity(
                        PdfViewerActivity.launchPdfFromUrl(
                            context,
                            documentCardResponse.documentPathURL,
                            "Onlive docs",
                            "onlive/pdf",
                        )
                    )
                }
            }
            DocumentTypes.WEB.title -> {
                isDocumentAnImage(false)
                binding.imageDocumentPreview.setOnClickListener {
                    showWebDocumentInCustomTab(documentCardResponse.documentPathURL)
                }
            }
            DocumentTypes.IMAGE.title -> {
                isDocumentAnImage(true)
                binding.imgDetailsProductPhoto.loadImage(
                    documentCardResponse.documentPathURL
                )
            }
        }
    }

    private fun isDocumentAnImage(isImage: Boolean) {
        if (isImage) {
            binding.imgDetailsProductPhoto.visible()
            binding.imageDocumentPreview.gone()
        } else {
            binding.imageDocumentPreview.visible()
            binding.imgDetailsProductPhoto.gone()
        }
    }

    private fun showWebDocumentInCustomTab(url: String?) {
        url ?: return
        val customTabsIntent = CustomTabsIntent.Builder()
            .addDefaultShareMenuItem()
            .setToolbarColor(getColor(R.color.colorPrimary))
            .setShowTitle(true)
            .build()

        addKeepAliveExtra(mainActivity, customTabsIntent.intent)

        openCustomTab(
            mainActivity, customTabsIntent,
            Uri.parse(url),
            WebViewFallback()
        )
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }
}