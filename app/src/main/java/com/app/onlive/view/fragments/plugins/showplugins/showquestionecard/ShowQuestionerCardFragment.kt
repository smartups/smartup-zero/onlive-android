package com.app.onlive.view.fragments.plugins.showplugins.showquestionecard

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.text.HtmlCompat
import androidx.fragment.app.viewModels
import com.app.onlive.R
import com.app.onlive.base.BaseFragment
import com.app.onlive.databinding.FragmentShowQuestionerCardPluginBinding
import com.app.onlive.extensions.backPress
import com.app.onlive.extensions.gone
import com.app.onlive.extensions.showToastMsg
import com.app.onlive.models.response.QuestionerCardResponse
import com.app.onlive.utils.Constants.CARD_ID

class ShowQuestionerCardFragment : BaseFragment(), ShowQuestionerCardViewModel.View,
    View.OnClickListener {

    private lateinit var binding: FragmentShowQuestionerCardPluginBinding
    private val viewModel: ShowQuestionerCardViewModel by viewModels()
    private var cardId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShowQuestionerCardPluginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.attachView(this)

        cardId = arguments?.getString(CARD_ID)
        if (cardId == null) {
            showToastMsg("Something went wrong")
            mainActivity.backPress()
            return
        }

        viewModel.getCardDetails(cardId!!)
        mainActivity.binding.appBar.btnBack.setOnClickListener(this@ShowQuestionerCardFragment)

        with(binding) {
            btnGoBack.setOnClickListener(this@ShowQuestionerCardFragment)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back, R.id.btnGoBack -> mainActivity.backPress()
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCardDetails(questionerCardResponse: QuestionerCardResponse) {
        binding.tvQuestionerDetails.text = questionerCardResponse.questionerQuestion

        binding.tvItemTitle1.text = Html.fromHtml(
            questionerCardResponse.questionerOption1?.value +
                    "<font color=red> (" + questionerCardResponse.questionerOption1?.count + ")</font>",
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.tvItemTitle2.text = Html.fromHtml(
            questionerCardResponse.questionerOption2?.value +
                    "<font color=red> (" + questionerCardResponse.questionerOption2?.count + ")</font>",
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.tvItemTitle3.text = Html.fromHtml(
            questionerCardResponse.questionerOption3?.value +
                    "<font color=red> (" + questionerCardResponse.questionerOption3?.count + ")</font>",
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.tvItemTitle4.text = Html.fromHtml(
            questionerCardResponse.questionerOption4?.value +
                    "<font color=red> (" + questionerCardResponse.questionerOption4?.count + ")</font>",
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )

        onCastingVote(questionerCardResponse)
    }

    fun onCastingVote(questionerCardResponse: QuestionerCardResponse) {
        with(binding) {
            radioBtnItem1.setOnClickListener {
                viewModel.castVoteForQuestioner(
                    cardId!!,
                    "questionerOption1",
                    questionerCardResponse.questionerOption1?.value.toString(),
                    questionerCardResponse.questionerOption1?.count!!
                )
            }
            radioBtnItem2.setOnClickListener {
                viewModel.castVoteForQuestioner(
                    cardId!!,
                    "questionerOption2",
                    questionerCardResponse.questionerOption2?.value.toString(),
                    questionerCardResponse.questionerOption2?.count!!
                )
            }
            radioBtnItem3.setOnClickListener {
                viewModel.castVoteForQuestioner(
                    cardId!!,
                    "questionerOption3",
                    questionerCardResponse.questionerOption3?.value.toString(),
                    questionerCardResponse.questionerOption3?.count!!
                )
            }
            radioBtnItem4.setOnClickListener {
                viewModel.castVoteForQuestioner(
                    cardId!!,
                    "questionerOption4",
                    questionerCardResponse.questionerOption4?.value.toString(),
                    questionerCardResponse.questionerOption4?.count!!
                )
            }
        }
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    override fun onVoteSuccess(message: String) {

        with(binding) {
            radioBtnItem1.gone()
            radioBtnItem2.gone()
            radioBtnItem3.gone()
            radioBtnItem4.gone()
        }
        viewModel.getCardDetails(cardId!!)
    }
}