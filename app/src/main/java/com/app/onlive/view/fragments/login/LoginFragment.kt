package com.app.onlive.view.fragments.login

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.app.onlive.base.BaseLoginFragment
import com.app.onlive.databinding.FragmentLoginBinding
import com.app.onlive.extensions.getRunTimePermissions
import com.app.onlive.extensions.replaceFragment
import com.app.onlive.extensions.showToastMsg
import com.app.onlive.utils.LocationPermissions
import com.app.onlive.view.fragments.signup.SignupFragment
import com.google.firebase.auth.FirebaseUser
import com.permissionx.guolindev.PermissionX

class LoginFragment : BaseLoginFragment(), LoginViewModel.View, LocationPermissions.LocationPermissionCallBack {

    private val viewModel: LoginViewModel by viewModels()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        LocationPermissions.checkLocationPermissions(loginActivity, this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            attachView(this@LoginFragment)
        }
        getRunTimePermissions()

        binding.btnSignup.setOnClickListener {
            replaceFragment(SignupFragment(), addToBackStack = false)
        }

        binding.btConfirm.setOnClickListener {
            viewModel.login(
                loginActivity,
                binding.etEmail.text.toString(),
                binding.etPassword.text.toString()
            )
            viewModel.getProfile()
        }
    }

    override fun showProgressBar() {
        progressDialog.show()
    }

    override fun dismissProgressBar() {
        progressDialog.dismiss()
    }

    override fun onLoginSuccess(firebaseUser: FirebaseUser, message: String) {
        goToMainActivity()
        showToastMsg(message)
    }

    override fun onDetailsUpdateError(message: String) {
        showToastMsg(message)
    }

    override fun onLocationPermissionChangeListener(permission: Boolean) {

    }
}