package com.app.onlive.models

open class BaseResponse {
    var statusCode: String? = null
    var statusMessage: String? = null
}