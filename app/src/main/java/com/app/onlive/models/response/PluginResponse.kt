package com.app.onlive.models.response

data class PluginResponse(
    var id: String? = null,
    var title: String? = null,
    var details: String? = null,
    var icon: String? = null,
    var createdBy: String? = null,
    var color: String? = null
)
