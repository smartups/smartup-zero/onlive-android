package com.app.onlive.models.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CardsResponse(
    var cardId: String? = null,
    var createdBy: String? = null,
    var cardIcon: String? = null,
    var cardState: String? = null,
    var cardTitle: String? = null,
    var isCardShareAble: Boolean = false,
    var cardGroupId: String? = null,
    var cardPluginId: String? = null,
    var cardPluginIcon: String? = null,
    var cardCreatorId: String? = null,
    var idCardResponse: IdCardResponse? = null,
    var documentCardResponse: DocumentCardResponse? = null,
    var questionerCardResponse: QuestionerCardResponse? = null,
) : Parcelable

@Parcelize
data class IdCardResponse(
    //data variants for the id card plugin
    var idCardName: String? = null,
    var idCardPhone: String? = null,
    var idCardEmail: String? = null,
    var idCardAddress: String? = null,
    var idCardImage: String? = null,
) : Parcelable

@Parcelize
data class DocumentCardResponse(
    //data variants for the document card plugin
    var documentDetails: String? = null,
    var documentPathURL: String? = null,
    var documentExtensionType: String? = null,
) : Parcelable

@Parcelize
data class QuestionerCardResponse(
    //data variants for the Questioner card plugin
    var questionerQuestion: String? = null,
    // while creating the card
    var questionerOption1: QuestionerCardOptions? = null,
    var questionerOption2: QuestionerCardOptions? = null,
    var questionerOption3: QuestionerCardOptions? = null,
    var questionerOption4: QuestionerCardOptions? = null,
) : Parcelable

@Parcelize
data class QuestionerCardOptions(
    var key: String? = "",
    var value: String = "",
    var count: Long = 0
) : Parcelable

