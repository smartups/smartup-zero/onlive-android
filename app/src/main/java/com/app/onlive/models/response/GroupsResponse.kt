package com.app.onlive.models.response

data class GroupsResponse(
    var id: String,
    var title: String? = null,
    var selectedPluginList: ArrayList<String>?= null,
    var details: String? = null,
    var icon: String? = null,
    var createdBy: String? = null,
    var groupLatitude: Double = 0.0,
    var groupLongitude: Double = 0.0,
    var geoFenceRadius: Double = 0.025,
)
