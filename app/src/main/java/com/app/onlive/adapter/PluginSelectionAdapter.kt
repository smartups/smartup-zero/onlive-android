package com.app.onlive.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.onlive.R
import com.app.onlive.extensions.gone
import com.app.onlive.extensions.loadImage
import com.app.onlive.extensions.visible
import com.app.onlive.models.response.PluginResponse
import com.mikhaellopez.circularimageview.CircularImageView

class PluginSelectionAdapter
    (private val onPluginSelection: OnPluginSelection, private val multiSelection: Boolean) :
    RecyclerView.Adapter<PluginSelectionAdapter.ViewHolder>() {

    private val pluginList: ArrayList<PluginResponse> = ArrayList()

    fun setItems(list: List<PluginResponse>) {
        list.apply {
            pluginList.clear()
            pluginList.addAll(this)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item_plugin, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plugin = pluginList[position]

        holder.tvPluginTitle.text = plugin.title
        holder.tvPluginDetails.text = plugin.details
        holder.imagePlugin.loadImage(plugin.icon)

        if (multiSelection) {
            holder.chkBoxItem.visible()
            holder.chkBoxItem.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    onPluginSelection.onItemCheckedListener(plugin)
                } else {
                    onPluginSelection.onItemUnCheckedListener(plugin)
                }
            }
        } else {
            holder.chkBoxItem.gone()
            holder.layoutContact.setOnClickListener {
                onPluginSelection.onItemSelectionListener(plugin)
            }
        }
    }

    override fun getItemCount(): Int {
        return pluginList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var layoutContact: LinearLayout = view.findViewById(R.id.layoutContact)
        var tvPluginTitle: TextView = view.findViewById(R.id.tvPluginTitle)
        var tvPluginDetails: TextView = view.findViewById(R.id.tvPluginDetails)
        var imagePlugin: CircularImageView = view.findViewById(R.id.imagePlugin)
        var chkBoxItem: CheckBox = view.findViewById(R.id.chkBoxItem)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        recyclerView.setItemViewCacheSize(itemCount)
    }

    interface OnPluginSelection {
        fun onItemCheckedListener(datum: PluginResponse)
        fun onItemUnCheckedListener(datum: PluginResponse)
        fun onItemSelectionListener(datum: PluginResponse)
    }
}