package com.app.onlive.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.onlive.R
import com.app.onlive.extensions.loadImage
import com.app.onlive.models.response.GroupsResponse
import com.mikhaellopez.circularimageview.CircularImageView

class HomeAdapter(private val setOnClickHandler: SetOnClickHandler) :
    RecyclerView.Adapter<HomeAdapter.MyViewHolder>() {

    private val groupList: ArrayList<GroupsResponse> = ArrayList()

    fun setItems(list: List<GroupsResponse>) {
        list.apply {
            groupList.clear()
            groupList.addAll(this)
            notifyDataSetChanged()
        }
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var userImage = view.findViewById(R.id.userImage) as CircularImageView
        var groupTitle = view.findViewById(R.id.username) as TextView
        var description = view.findViewById(R.id.status) as TextView
        var layoutGroup = view.findViewById(R.id.layoutContact) as LinearLayout
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_message_info, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return groupList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val group = groupList[position]

        holder.groupTitle.text = group.title
        holder.description.text = group.details
        holder.userImage.loadImage(group.icon)

        holder.layoutGroup.setOnClickListener {
            setOnClickHandler.onClickHandler(group)
        }
    }

    interface SetOnClickHandler {
        fun onClickHandler(group: GroupsResponse)
    }
}