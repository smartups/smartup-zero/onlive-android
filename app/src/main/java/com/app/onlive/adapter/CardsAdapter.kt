package com.app.onlive.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.onlive.R
import com.app.onlive.enums.Plugins
import com.app.onlive.extensions.gone
import com.app.onlive.extensions.loadImage
import com.app.onlive.extensions.visible
import com.app.onlive.models.response.CardsResponse
import com.mikhaellopez.circularimageview.CircularImageView

class CardsAdapter(private val onCardClickListener: OnCardClickListener) :
    RecyclerView.Adapter<CardsAdapter.MyViewHolder>() {

    private val cardList: ArrayList<CardsResponse> = ArrayList()

    fun setItems(list: List<CardsResponse>) {
        list.apply {
            cardList.clear()
            cardList.addAll(this)
            notifyDataSetChanged()
        }
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var layoutCard = view.findViewById(R.id.layoutCard) as LinearLayout
        var tvTitle = view.findViewById(R.id.tv_title) as TextView
        var CardImage = view.findViewById(R.id.iv_cardImg) as ImageView
        var layoutCreaterName = view.findViewById(R.id.layout_createrName) as LinearLayout
        var tvTime = view.findViewById(R.id.tv_time) as TextView
        var tvByName = view.findViewById(R.id.tv_byName) as TextView
        var civPlugin = view.findViewById(R.id.civ_plugin) as CircularImageView
        var downloadable = view.findViewById(R.id.downloadable) as ImageView
        var notDownloadable = view.findViewById(R.id.notDownloadable) as ImageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardsAdapter.MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_card, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CardsAdapter.MyViewHolder, position: Int) {
        val card = cardList[position]
        for (plugin in Plugins.values()) {
            if (card.cardPluginId == plugin.id) {
                holder.layoutCard.getBackground()
                    .setTint(Color.parseColor(plugin.bgColor))
                holder.layoutCreaterName.setBackgroundColor(Color.parseColor(plugin.lightBgColor))
            }
        }
        holder.tvTitle.text = card.cardTitle
        holder.tvByName.text = "By : ${card.createdBy}"
        holder.CardImage.loadImage(card.cardIcon)
        holder.civPlugin.loadImage(card.cardPluginIcon)

        if (card.isCardShareAble) {
            holder.downloadable.visible()
            holder.notDownloadable.gone()
        } else {
            holder.downloadable.gone()
            holder.notDownloadable.visible()
        }
        holder.layoutCard.setOnClickListener {
            onCardClickListener.clickListener(card)
        }
    }

    override fun getItemCount(): Int {
        return cardList.size
    }

    interface OnCardClickListener {
        fun clickListener(card: CardsResponse)
    }
}