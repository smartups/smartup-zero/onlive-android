package com.app.onlive.utils


object Constants {
    const val BASE_URL = "http://kotlin-webflux.herokuapp.com/pilot/"
    const val USER_SESSION = "USER_SESSION"
    const val USER_ID = "USER_ID"
    const val ACCESS_TOKEN = "ACCESS_TOKEN"
    const val EXTRA_GROUP_ID = "EXTRA_GROUP_ID"
    var isLogoutCalled = false
    const val IS_IMAGE_VIDEO = "isImageVideo"
    const val IS_DOCUMENT = "isDocument"
    const val CARD_MODEL = "CARD_MODEL"
    const val CARD_ID = "CARD_ID"
    const val QRCODE_INPUT = "QRCODE_INPUT"
    const val PLUGIN_MODEL = "PLUGIN_MODEL"
    const val USER_PROFILE = "USER_PROFILE"
    const val USER_EMAIL = "USER_EMAIL"
}