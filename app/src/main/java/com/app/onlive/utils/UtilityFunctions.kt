package com.app.onlive.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.content.ContextCompat
import com.app.onlive.R
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


/**
 * Created by Malik Dawar on 16/03/20.
 */
object UtilityFunctions {

    fun createNotificationId(): Int {
        val now = Date()
        return SimpleDateFormat("ddHHmmss", Locale.US).format(now).toInt()
    }

    /**
     * A method which test either password is valid or not.
     * @return Boolean : True, False.
     * @author Dawar Malik.
     */
    fun CharSequence?.isValidPassword() =
        !isNullOrEmpty() && this.length > 7 /*|| password.matches("[a-zA-Z0-9]+")*/

    /**
     * A method which test either email is valid or not.
     * @param email for validation.
     * @return Boolean : True, False.
     * @author Dawar Malik.
     */
    fun isValidEmail(email: String?): Boolean {
        return if (email.isNullOrBlank()) false
        else Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+").matcher(email).matches()
    }

    fun showPermissionMissing(activity: Activity, message: String?) {
        Snackbar.make(
            activity.findViewById(R.id.content),
            message!!,
            Snackbar.LENGTH_LONG
        ).setAction("Settings") {
            goToSettings(activity.applicationContext)
        }.show()
    }

    private fun goToSettings(context: Context) {
        val myAppSettings = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + context.packageName)
        )
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
        myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(myAppSettings)
    }

    fun isPermissionsGiven(activity: Activity, hasPermissions: (Boolean) -> Unit) {
        hasPermissions(
            ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        )
    }

    fun isUserInRadius(
        pointA: LatLng,
        pointB: LatLng,
        radius: Double = 0.03
    ): Boolean {
        return calculationByDistance(pointB, pointA) < radius
    }

    fun calculationByDistance(StartP: LatLng, EndP: LatLng): Double {
        val Radius = 6371
        val lat1 = StartP.latitude
        val lat2 = EndP.latitude
        val lon1 = StartP.longitude
        val lon2 = EndP.longitude
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a = sin(dLat / 2) * sin(dLat / 2) + (cos(Math.toRadians(lat1))
                * cos(Math.toRadians(lat2)) * sin(dLon / 2) * sin(dLon / 2))
        val c = 2 * asin(sqrt(a))
        return Radius * c
    }
}