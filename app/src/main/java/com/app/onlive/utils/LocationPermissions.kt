package com.app.onlive.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.app.onlive.App

object LocationPermissions {

    private const val REQUEST_CODE_LOCATION = 123

    fun checkLocationPermissions(
        activity: Activity,
        locationPermission: LocationPermissionCallBack
    ) {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (ContextCompat.checkSelfPermission(
                App.getAppContext(), Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                App.getAppContext(), Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermission.onLocationPermissionChangeListener(true)
            if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.Q) {
                requestBackgroundPermission(activity)
            }
        } else {
            ActivityCompat.requestPermissions(
                activity,
                permissions,
                REQUEST_CODE_LOCATION
            )

            ifLocationPermissionsGiven(activity) {
                locationPermission.onLocationPermissionChangeListener(it)
            }
        }
    }

    fun ifLocationPermissionsGiven(activity: Activity, hasPermissions: (Boolean) -> Unit) {
        hasPermissions(
            ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        )
    }

    const val REQUEST_CODE_BACKGROUND = 1545
    @RequiresApi(Build.VERSION_CODES.Q)
    fun requestBackgroundPermission(activity: Activity) {

        val hasForegroundLocationPermission = ActivityCompat.checkSelfPermission(activity.applicationContext,
            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED

        if (hasForegroundLocationPermission) {
            val hasBackgroundLocationPermission = ActivityCompat.checkSelfPermission(activity.applicationContext,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED

            if (hasBackgroundLocationPermission) {
                // handle location update
            } else {
                ActivityCompat.requestPermissions(activity,
                    arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION), REQUEST_CODE_BACKGROUND)
            }
        } else {
            ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION), REQUEST_CODE_BACKGROUND)
        }
    }

    interface LocationPermissionCallBack {
        fun onLocationPermissionChangeListener(permission: Boolean)
    }
}