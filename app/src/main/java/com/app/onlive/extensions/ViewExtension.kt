package com.app.onlive.extensions

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.app.onlive.App
import com.app.onlive.listeners.SingleClickListener
import com.permissionx.guolindev.PermissionX
import net.glxn.qrgen.android.QRCode
import java.io.File

/**
 * Click Extension function works as listener setter that prevents double click on the view it´s set
 * @return view
 * @author Dawar Malik.
 */
fun View.singleClick(l: (View?) -> Unit) {
    setOnClickListener(SingleClickListener(l))
}

/**
 * Extension function to show toast message
 * @return void
 * @author Dawar Malik.
 */
fun Any.showToastMsg(message: String) {
    Toast.makeText(App.getAppContext(), message, Toast.LENGTH_SHORT).show()
}

/**
 * An Extension to close keyboard.
 * @return void
 * @author Dawar Malik.
 */
fun View.closeKeyboard() {
    val imm =
        this.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

/**
 * An Extension to Show keyboard
 * @return void
 * @author Dawar Malik.
 */
fun View.showKeyBoard() {
    val imm = this.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

/**
 * An Extension to make view Visible
 * @return void
 * @author Dawar Malik.
 */
fun View.visible() {
    visibility = View.VISIBLE
}

/**
 * An Extension to make view InVisible
 * @return void
 * @author Dawar Malik.
 */
fun View.invisible() {
    visibility = View.INVISIBLE
}

/**
 * An Extension to make view Gone
 * @return void
 * @author Dawar Malik.
 */
fun View.gone() {
    visibility = View.GONE
}

/**
 * An Extension to getColorCompat of view via context
 * @return void
 * @author Dawar Malik.
 */
fun Context.getColorCompat(@ColorRes colorRes: Int) = ContextCompat.getColor(this, colorRes)

/**
 * An Extension to getColorCompat via fragment
 * @return void
 * @author Dawar Malik.
 */
fun Fragment.getColor(@ColorRes colorRes: Int) = ContextCompat.getColor(requireContext(), colorRes)

/**
 * An Extension to start Activity
 * @return void
 * @author Dawar Malik.
 */
fun Activity.startActivityNewTask(clazz: Class<*>) {
    val intent = Intent(this, clazz)
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
}

/**
 * An Extension to LoadImage
 * @return void
 */
fun ImageView.loadImage(url: String?) {
    load(url)
}

fun String.generateQRCode(): Bitmap {
    return QRCode.from(this).bitmap()
}

fun Context.showOptionsSelectionDialogue(optionList: Array<String>, position: (Int) -> Unit) {
    val builder = AlertDialog.Builder(this)
    builder.setItems(
        optionList
    ) { dialog, which ->
        position(which)
        dialog.dismiss()
    }
    val dialog = builder.create()
    dialog.window?.setGravity(Gravity.CENTER)
    val window = dialog.window
    window?.setLayout(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
    )
    dialog.show()
}


fun Fragment.getRunTimePermissions(){
    PermissionX.init(this)
        .permissions( Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)
        .request { _, _, _ ->

        }
}