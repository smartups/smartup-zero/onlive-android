package com.app.onlive.repository

import android.net.Uri
import com.app.onlive.base.BaseRepository
import com.app.onlive.view.activities.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.*
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage

/**
 *  The repository handling database operations for authentication flow.
 */
class PlatformRepository private constructor() : BaseRepository() {

    private lateinit var storage: FirebaseStorage
    private var auth: FirebaseAuth? = null
    private val fireStoreDatabase = FirebaseFirestore.getInstance()
    private fun initFirebaseAuth() {
        auth = Firebase.auth
    }

    fun getUserAuth(): FirebaseAuth {
        if (auth == null)
            initFirebaseAuth()
        return auth!!
    }

    fun createAccount(
        loginActivity: LoginActivity,
        email: String,
        password: String,
        isSuccessful: (FirebaseUser?, String?) -> Unit
    ) {
        if (auth == null)
            initFirebaseAuth()

        auth!!.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(loginActivity) { task ->
                if (task.isSuccessful) {
                    isSuccessful(auth?.currentUser, null)
                } else {
                    isSuccessful(null, task.exception?.localizedMessage!!)
                }
            }
    }

    fun signIn(
        loginActivity: LoginActivity,
        email: String,
        password: String,
        isSuccessful: (FirebaseUser?, String?) -> Unit
    ) {
        if (auth == null)
            initFirebaseAuth()

        auth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(loginActivity) { task ->
                if (task.isSuccessful) {
                    isSuccessful(auth?.currentUser, null)
                } else {
                    isSuccessful(null, task.exception?.localizedMessage)
                }
            }
    }

    fun saveDataInFireBase(
        collectionPath: String,
        hashMap: HashMap<String, Any>,
        fireStoreListener: (DocumentReference?, String?) -> Unit
    ) {
        fireStoreDatabase.collection(collectionPath)
            .add(hashMap)
            .addOnSuccessListener {
                fireStoreListener.invoke(it, null)
            }
            .addOnFailureListener { exception ->
                fireStoreListener.invoke(null, exception.localizedMessage)
            }
    }

    fun updateDataInFireBase(
        collectionPath: String,
        documentPath: String,
        hashMap: HashMap<String, Any>,
        fireStoreListener: (Boolean, String) -> Unit
    ) {
        fireStoreDatabase.collection(collectionPath).document(documentPath)
            .set(hashMap, SetOptions.merge())
            .addOnSuccessListener {
                fireStoreListener.invoke(true, "Updated successfully")
            }
            .addOnFailureListener { exception ->
                fireStoreListener.invoke(
                    false,
                    exception.localizedMessage ?: "Something went wrong"
                )
            }
    }

    fun deleteDocumentById(
        collectionPath: String,
        documentPathId: String,
        fireStoreListener: (Boolean, String) -> Unit
    ) {
        fireStoreDatabase.collection(collectionPath).document(documentPathId)
            .delete()
            .addOnSuccessListener { fireStoreListener.invoke(true, "deleted successfully") }
            .addOnFailureListener { exception ->
                fireStoreListener.invoke(
                    false,
                    exception.localizedMessage ?: "Something went wrong"
                )
            }
    }

    fun getDataFromFireBase(
        collectionPath: String,
        searchKey: String? = null,
        searchValue: String? = null,
        fireStoreListener: (QuerySnapshot?, String?) -> Unit
    ) {
        val collections = fireStoreDatabase.collection(collectionPath)
        if (searchKey.isNullOrBlank().not()) {
            collections.whereEqualTo(searchKey!!, searchValue)
        }
        collections
            .get().addOnSuccessListener { querySnapshot ->
                fireStoreListener(querySnapshot, null)
            }
            .addOnFailureListener { exception ->
                fireStoreListener(null, exception.localizedMessage)
            }
    }

    fun getDataFromFireBaseById(
        collectionPath: String,
        selectedId: String? = null,
        fireStoreListener: (DocumentSnapshot?, String?) -> Unit
    ) {
        val docRef = fireStoreDatabase.collection(collectionPath).document(selectedId!!)
        docRef.get()
            .addOnSuccessListener { document ->
                fireStoreListener.invoke(document, null)
                if (document != null) {
                    fireStoreListener.invoke(document, null)
                } else {
                    fireStoreListener.invoke(null, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                fireStoreListener.invoke(null, exception.localizedMessage)
            }

    }

    fun uploadFileToFireBase(
        uri: Uri,
        onSuccess: (imgPath: String?, error: String?) -> Unit
    ) {
        if (auth == null)
            initFirebaseAuth()

        storage = Firebase.storage

        val ref = storage.reference.child(auth?.currentUser?.uid.toString())
            .child(uri.lastPathSegment.toString())

        val uploadTask = ref.putFile(uri)
        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            ref.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                onSuccess.invoke(downloadUri.toString(), null)
            } else {
                onSuccess.invoke(null, "Something went wrong")
            }
        }.addOnFailureListener {
            onSuccess.invoke(null, it.localizedMessage)
        }
    }

    companion object {
        private var instance: PlatformRepository? = null
        fun getInstance(): PlatformRepository {
            if (instance == null)
                instance = PlatformRepository()
            return instance!!
        }
    }
}