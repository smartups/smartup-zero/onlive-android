Deckpack Android app using normal communication infrastructre. To be replaced with Onlive mesh protocol. 


Product description
What does Deckpack do (in its adult form)?


1: Grouping people within the dynamics of physical shared spaces and the common resources available there, e.g. a group of people that:
Are in the same room 				
Work in the same office				
Shop in the same store				
Are at the same event 				VERSION 1.0
Live in the same flat				
Are on the same public square			
Live in the same street				

2: Administration of the mutual exchange of relevant information between humans, computers and group hosts that are the stakeholders in that physical context.
Mesh-networking to distribute data among group members (and their smart devices).
POC does not need a mesh implemented and operationable but has a ‘normal’ client/server set up that simulates mesh. That way we can implement mesh in a later stage. Mesh is on the R&D roadmap.
Context rules to determine relevance for the group (and their smart devices)
Fully functioning. 
Plugin runtime to execute web applications inside the groups
Fully functioning.

3: Facilitating functionality for:
Disaster control services (Deckpack works where other communication infrastructrures fail).
Virus control, i.e
Group control and monitoring, e.g:
Warning system for crowded groups
Heatmap for local area’s around the user
Entree policy for bars and clubs
Social distance information
Post diaster communications, i.e
Real time information from communicities in need of help
Organizational tools to share resources in communities
Sharing social information about loses and founds of people and goods. 
Collaboratively enriching information:
Group organization and planning, e.g:
Filling in a poll at a convention
Working in the same document in a meeting.
Report local incident (e.g. a bike being stolen)
A better collective experience in our physical spaces.
Using local available digital services, e.g:
Local payment system in a restaurant
City guide system for tourists in a city
Using local available smart devices, e.g:
Controlling the available smart tv in the room
Controlling the music system in a bar (jukebox)
Using local available IoT data (smog alert system)
Sharing local common resources, e.g:
Local car sharing 
Local food sharing 
Local energy sharing
Connecting demand and supply of services between people and organizations 
Sharing supply, e.g:
Offer your time to help somebody in your neighbourhood
Offer professional services to organizations/people in your neighbourhood
Offer commercial opportunity to people in a specific physical context
Sharing demand, e.g:
Request help with getting groceries 
Request discussion about an idea
Request the best commercial deals from your local businesses.







